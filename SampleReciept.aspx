﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeBehind="SampleReciept.aspx.cs" Inherits="MigasLevelUp.SampleReciept" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .samplereceiptpage {
            padding-top:11.5em;
        }
        .tblsample {
            margin-left:2em;
        }
        .gx {
            margin-right:2em;
        }
        .hx {
            margin-bottom:0.5em;
            width:180px;
            height:40px;
            border:1px ridge lightgrey;
        }
            .hx:hover {
                border:1px ridge grey;
            }
        .srh4 {
            text-align:center;
            padding-top:0.5em;
            padding-bottom:0.5em;
            background-color:lightgray;
        }
        .sec {
            margin-left:2em;
            margin-bottom:2em;
        }
        .lto {
            margin-right:2em;
            margin-bottom:1em;
        }
        .lto1 {
            margin-right:2em;
            margin-left:1em;
        }
        .lty {
             width:240px;
            height:40px;
            border:1px ridge lightgrey;
            margin-bottom:0.5em;
            padding-left:0.5em;
        }
            .lty:hover {
                 border:1px ridge grey;
            }
        .sss {
            margin-right:6em;
            
        }
        .ltr {
            width:240px;
            height:40px;
            border:1px ridge lightgrey;
            padding-left:0.5em;
            margin-right:1em;
            margin-top:1em;
            margin-bottom:1em;
        }
            .ltr:hover {
                border:1px ridge grey;
            }
        .ltr1 {
            width:240px;
            height:40px;
            border:1px ridge lightgrey;
            padding-left:0.5em;
            margin-bottom:1em;
        }
        .ltr1:hover {
            border:1px ridge grey;
        }
        .ltr2 {
            width:240px;
            height:40px;
            border:1px ridge lightgrey;
            padding-left:0.5em;
           
        }
        .ltr2:hover {
            border:1px ridge grey;
        }
        .arrowbtn {
            position:absolute;
           margin-left:83em;
           margin-top:-6.5em;
        }
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="samplereceiptpage">
        <table class="tblsample">
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Truck/Vessel" CssClass="gx"></asp:Label></td>
                <td>
                    <asp:DropDownList ID="DropDownList1" runat="server" AppendDataBoundItems="true" CssClass="hx">
                        <asp:ListItem Text="Silahkan Pilih" Value="-1"></asp:ListItem>
                    </asp:DropDownList></td>
                <%--<td>
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox></td>--%>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Cargo/Muatan" CssClass="gx"></asp:Label></td>
                <td>
                    <asp:DropDownList ID="DropDownList2" runat="server" AppendDataBoundItems="true" CssClass="hx">
                        <asp:ListItem Text="Silahkan Pilih" Value="-1"></asp:ListItem>
                    </asp:DropDownList>
                </td>
               <%-- <td>
                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox></td>--%>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Port/Pelabuhan" CssClass="gx"></asp:Label></td>
                <td>
                    <asp:DropDownList ID="DropDownList3" runat="server" AppendDataBoundItems="true" CssClass="hx">
                        <asp:ListItem Text="Silahkan Pilih" Value="-1"></asp:ListItem>
                    </asp:DropDownList></td>
<%--                <td>
                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox></td>--%>
            </tr>
        </table>
        <h4 class="srh4">SAMPLE RECEIPT / Tanda Terima Engineer</h4>
        <section class="sec">
        <h5>AT DISCHARGE / LOADING</h5>
        <h5> Saat Pembongkarang / Penerimaan</h5><br />
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label4" runat="server" Text="To/Kepada" CssClass="lto"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox4" runat="server" CssClass="lty"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="At the Installation of" CssClass="lto"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox5" runat="server" CssClass="lty"></asp:TextBox></td>
                <td>
                    <asp:Label ID="Label6" runat="server" Text="On" CssClass="lto1"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox6" runat="server" CssClass="lty"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label7" runat="server" Text="Pada Instalasi di"></asp:Label></td>
                <td></td>
                <td>
                    <asp:Label ID="Label8" runat="server" Text="Tanggal" CssClass="lto1"></asp:Label></td>
            </tr>
        </table>
            <br />
        <h5>Please accept the following samples / mohon di terima sampel berikut</h5>
            <table class="tblsource">
                <tr>
                    <td>
                        <asp:Label ID="Label11" runat="server" Text="SAMPLE SOURCE<br />Sumber Sampel" CssClass="sss"></asp:Label></td>
                    <td>
                        <asp:Label ID="Label12" runat="server" Text="QUANTITY<br />Kuantitas"></asp:Label></td>
                    <td>
                        <asp:Label ID="Label13" runat="server" Text="SEAL NO.<br />Nomor Segel"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label14" runat="server" Text="Individual/Composite tank<br />Truck/Berge/Vessel"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="TextBox1" runat="server" Text="1 x 1 Liter" CssClass="ltr"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="TextBox2" runat="server" Text="Sealed No" CssClass="ltr"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label9" runat="server" Text="Representing a cargo of<br/>Mewakili Jenis Kargo"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="TextBox3" runat="server" Text="JE-SOLAR/JE-DISEL/JE-FUEL" CssClass="ltr1"></asp:TextBox></td>
                    <td>
                        <asp:Label ID="Label10" runat="server" Text="Transferred from Vessel<br/>di transfer dari kapal"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="TextBox7" runat="server" CssClass="ltr1"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label15" runat="server" Text="at (time)<br/>pukul"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="TextBox8" runat="server" CssClass="ltr2"></asp:TextBox></td>
                    <td>
                        <asp:Label ID="Label16" runat="server" Text="on (date)<br/>tanggal"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="TextBox9" runat="server" CssClass="ltr2"></asp:TextBox></td>
                </tr>
            </table>
            <asp:ImageButton ID="ImageButton1" runat="server" CssClass="arrowbtn" OnClick="ImageButton1_Click" ImageUrl="~/assets/img/Arrow.png" Width="50" Height="50" ToolTip="Go to Bunker Check List Page" />
      </section>
    </div>
</asp:Content>
