﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeBehind="FlowMeter.aspx.cs" Inherits="MigasLevelUp.FlowMeter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        /* div = FMdiv*/
        .flowmeterpage {
            padding-top:11.5em;
        }
        .FMdiv {
            margin-left:2em;
        }
        .lbl1 {
            margin-right:8em;
        }
        .dd1 {
            margin-bottom:0.5em;
            width:180px;
            height:40px;
            border:1px ridge lightgrey;
            
        }
            .dd1:hover {
                 border:1px ridge grey;
            }
        .dd2 {
            margin-bottom:0.5em;
            width:180px;
            height:40px;
            border:1px ridge lightgrey;
        }
         .dd2:hover {
                 border:1px ridge grey;
            }
        .dd3 {
            margin-bottom:0.5em;
            width:180px;
            height:40px;
            border:1px ridge lightgrey;
        }
         .dd3:hover {
                 border:1px ridge grey;
            }
        .dd4 {
            width:180px;
            height:40px;
            border:1px ridge lightgrey;
        }
         .dd4:hover {
                 border:1px ridge grey;
            }
         /* section = sec*/
        .sec {
            margin-left:2em;
            margin-right:2em;
        }
        .hfour {
            background-color:lightgrey;
            padding-top:0.5em;
            padding-bottom:0.5em;
            text-align:center;
        }
        .tblflowmeter {
            width:100%;
        }
        .tblflowmeter tr td {
            border:1px ridge lightgrey;
        }
        .vu1 {
            margin-right:1em;
            margin-left:1em;
        }
        .vu2 {
            margin-right:1em;
            margin-left:1em;
        }
        .vu3 {
            margin-right:1em;
            margin-left:1em;
        }
        .vu4 {
            margin-right:1em;
            margin-left:1em;
        }
        .wu1 {
             width:160px;
            height:40px;
            border:1px ridge lightgrey;
            padding-left:0.5em;
            margin-right:1em;
        }
            .wu1:hover {
                   border:1px ridge grey;
            }
        .wu2 {
            margin-right: 1em;
             width:160px;
            height:40px;
            border:1px ridge lightgrey;
            padding-left:0.5em;
        }
            .wu2:hover {
                   border:1px ridge grey;
            }
        .wu3 {
             margin-right: 1em;
              width:160px;
            height:40px;
            border:1px ridge lightgrey;
            padding-left:0.5em;
        }
            .wu3:hover {
                   border:1px ridge grey;
            }
        .wu4 {
             width:160px;
            height:40px;
            border:1px ridge lightgrey;
            padding-left:0.5em;
            margin-top:1em;
            margin-right:1em;
            margin-bottom:1em;
        }
            .wu4:hover {
                   border:1px ridge grey;
            }
        .tu1 {
            margin-left:1em;
            margin-right:1.4em;
        }
        .tu2 {
             margin-left:1em;
            margin-right:1em;
        }
        .tu3 {
             margin-left:1em;
            margin-right:1em;
        }
        .tx1 {
            margin-top:1em;
            margin-bottom:1em;
            width:160px;
            height:40px;
            border:1px ridge lightgrey;
            padding-left:0.5em;
        }
            .tx1:hover {
                   border:1px ridge grey;
            }
        .tx2 {
            width:240px;
            height:40px;
            border:1px ridge lightgrey;
            padding-left:0.5em;
        }
            .tx2:hover {
                border:1px ridge grey;
            }
        .tx3 {
            width:240px;
            height:40px;
            border:1px ridge lightgrey;
            padding-left:0.5em;
        }
            .tx3:hover {
                border:1px ridge grey;
            }
        .sm1 {
            margin-left:1em;
            margin-bottom:1em;
            margin-top:1em;
        }
        .sn1 {
            margin-left:1em;
            margin-bottom:1em;
            margin-top:1em;
            width:290px;
            height:40px;
            border:1px ridge lightgrey;
            padding-left:0.5em;
        }
            .sn1:hover {
                border:1px ridge grey;
            }

             /* section = sec1*/

        .sec1 {
            margin-left:2em;
            margin-right:2em;
        }
        .tblpumprecord {
            width:100%;
        }
            .tblpumprecord tr td {
                border:1px ridge lightgrey;
                text-align:center;
                height:30px;
            }
        .remark {
            margin-left:2em;
        }
        .txtremark {
            margin-left:2em;
            margin-right:2em;
            width:95%;
            padding-left:0.5em;
        }
        .tblcargo {
            margin-left:2em;
            margin-bottom:2em;
        }
        .co1 {
            width:240px;
            height:40px;
            border:1px ridge lightgrey;
            padding-left:0.5em;
            margin-right:5em;
            margin-bottom:1em;
        }
        .co1:hover {
            border:1px ridge grey;
        }
        .co2 {
            width:240px;
            height:40px;
            border:1px ridge lightgrey;
            padding-left:0.5em;
            margin-bottom:1em;
        }
            .co2:hover {
                border:1px ridge grey;
            }
        .arrowbtn {
            position:absolute;
           margin-left:83em;
           margin-top:-6.5em;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="flowmeterpage">
    <div class="FMdiv">
    <table>
        <tr>
            <td><asp:Label ID="Label1" runat="server" Text="Vessel" CssClass="lbl1"></asp:Label></td>
            <td><asp:DropDownList ID="DropDownList1" runat="server" AppendDataBoundItems="true" CssClass="dd1">
                <asp:ListItem Text="Silahkan Pilih" Value="-1"></asp:ListItem>
                </asp:DropDownList></td>
            <%--<td><asp:Label ID="Label5" runat="server" Text="Label"></asp:Label></td>--%>
            
            
        </tr>
        <tr>
            <td> <asp:Label ID="Label2" runat="server" Text="Bunker Tanker/ Berge" CssClass="lbl2"></asp:Label></td>
            <td>
                <asp:DropDownList ID="DropDownList2" runat="server" AppendDataBoundItems="true" CssClass="dd2">
                    <asp:ListItem Text="Silahkan Pilih" Value="-1"></asp:ListItem>
                </asp:DropDownList></td>
            <%--<td><asp:Label ID="Label6" runat="server" Text="Label"></asp:Label></td>--%>
        </tr>
        <tr>
            <td><asp:Label ID="Label3" runat="server" Text="Cargo" CssClass="lbl3"></asp:Label></td>
            <td>
                <asp:DropDownList ID="DropDownList3" runat="server" AppendDataBoundItems="true" CssClass="dd3">
                    <asp:ListItem Text="Silahkan Pilih" Value="-1"></asp:ListItem>
                </asp:DropDownList></td>
            <%--<td><asp:Label ID="Label7" runat="server" Text="Label"></asp:Label></td>--%>
        </tr>
        <tr>
            <td><asp:Label ID="Label4" runat="server" Text="Port" CssClass="lbl4"></asp:Label></td>
            <td>
                <asp:DropDownList ID="DropDownList4" runat="server" AppendDataBoundItems="true" CssClass="dd4">
                    <asp:ListItem Text="Silahkan Pilih" Value="-1"></asp:ListItem>
                </asp:DropDownList></td>
            <%--<td><asp:Label ID="Label8" runat="server" Text="Label"></asp:Label></td>--%>
        </tr>
    </table>
        </div>
    <h4 class="hfour">FLOW METER MEASUREMENT DATA</h4>
    <section class="sec">
        
        <table class="tblflowmeter">
            <tr>
                <td>
                    <asp:Label ID="Label9" runat="server" Text="Volume Unit" CssClass="vu1"></asp:Label><asp:TextBox ID="TextBox1" runat="server" CssClass="wu1"></asp:TextBox></tdro>
                <td>
                    <asp:Label ID="Label10" runat="server" Text="Weight Unit " CssClass="vu2"></asp:Label><asp:TextBox ID="TextBox2" runat="server" CssClass="wu2"></asp:TextBox></td>
                <td>
                    <asp:Label ID="Label11" runat="server" Text="Flow Meter No. " CssClass="vu3"></asp:Label><asp:TextBox ID="TextBox3" runat="server" CssClass="wu3"></asp:TextBox></td>
                <td>
                    <asp:Label ID="Label12" runat="server" Text="Flow Meter No. " CssClass="vu4"></asp:Label><asp:TextBox ID="TextBox4" runat="server" CssClass="wu4"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label13" runat="server" Text="Temp. Unit " CssClass="tu1"></asp:Label><asp:TextBox ID="TextBox5" runat="server" CssClass="tx1"></asp:TextBox></td>
                
                <td>
                    <asp:Label ID="Label14" runat="server" Text="Date" CssClass="tu2"></asp:Label><asp:TextBox ID="TextBox49" runat="server" CssClass="tx2"></asp:TextBox></td>
                <td><asp:Label ID="Label15" runat="server" Text="Date" CssClass="tu3"></asp:Label><asp:TextBox ID="TextBox50" runat="server" CssClass="tx3"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label16" runat="server" Text="Meter System / Make" CssClass="sm1"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox6" runat="server" CssClass="sn1"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="TextBox7" runat="server" CssClass="sn1"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label17" runat="server" Text="Manufacture" CssClass="sm1"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox8" runat="server" CssClass="sn1"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="TextBox9" runat="server" CssClass="sn1"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label18" runat="server" Text="Calibration Date" CssClass="sm1"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox10" runat="server" CssClass="sn1"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="TextBox11" runat="server" CssClass="sn1"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label19" runat="server" Text="Capacity" CssClass="sm1"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox12" runat="server" CssClass="sn1"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="TextBox13" runat="server" CssClass="sn1"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label20" runat="server" Text="Flow Rate" CssClass="sm1"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox14" runat="server" CssClass="sn1"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="TextBox15" runat="server" CssClass="sn1"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label21" runat="server" Text="Initial Read Out" CssClass="sm1"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox16" runat="server" CssClass="sn1"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="TextBox17" runat="server" CssClass="sn1"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label22" runat="server" Text="Final Read Out" CssClass="sm1"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox18" runat="server" CssClass="sn1"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="TextBox19" runat="server" CssClass="sn1"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label23" runat="server" Text="Difference" CssClass="sm1"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox20" runat="server" CssClass="sn1"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="TextBox21" runat="server" CssClass="sn1"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label24" runat="server" Text="Meter Factor" CssClass="sm1"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox22" runat="server" CssClass="sn1"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="TextBox23" runat="server" CssClass="sn1"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label25" runat="server" Text="Gross Observed Volume Delivered" CssClass="sm1"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox24" runat="server" CssClass="sn1"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="TextBox25" runat="server" CssClass="sn1"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label26" runat="server" Text="Temp. Unit : °C" CssClass="sm1"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox26" runat="server" CssClass="sn1"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="TextBox27" runat="server" CssClass="sn1"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label27" runat="server" Text="Density 15 °C" CssClass="sm1"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox28" runat="server" CssClass="sn1"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="TextBox29" runat="server" CssClass="sn1"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label28" runat="server" Text="Volume Correction Faktor (V.C.F), tab 54B" CssClass="sm1"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox30" runat="server" CssClass="sn1"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="TextBox31" runat="server" CssClass="sn1"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label5" runat="server" Text="Gross Standart Volume Delivered" CssClass="sm1"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox32" runat="server" CssClass="sn1"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="TextBox33" runat="server" CssClass="sn1"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label29" runat="server" Text="Weight Correction Faktor (W.C.F), tab. 56" CssClass="sm1"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox34" runat="server" CssClass="sn1"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="TextBox35" runat="server" CssClass="sn1"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label30" runat="server" Text="Metric Tons" CssClass="sm1"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox36" runat="server" CssClass="sn1"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="TextBox37" runat="server" CssClass="sn1"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label31" runat="server" Text="Conversion Table (table 01)" CssClass="sm1"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox38" runat="server" CssClass="sn1"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="TextBox39" runat="server" CssClass="sn1"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label32" runat="server" Text="Long Tons" CssClass="sm1"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox40" runat="server" CssClass="sn1"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="TextBox41" runat="server" CssClass="sn1"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label33" runat="server" Text="Bbls (Table 52)" CssClass="sm1"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox42" runat="server" CssClass="sn1"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="TextBox43" runat="server" CssClass="sn1"></asp:TextBox></td>
            </tr>
             <tr>
                <td colspan="2">
                    <asp:Label ID="Label34" runat="server" Text="US Bbls @60°F" CssClass="sm1"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox44" runat="server" CssClass="sn1"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="TextBox45" runat="server" CssClass="sn1"></asp:TextBox></td>
            </tr>
        </table>
    </section><br />
   <section class="sec1">
       <h4>Pumping Record</h4>
       <table class="tblpumprecord">
           <tr class="trpump">
               <td>
                   <asp:Label ID="Label35" runat="server" Text="No." CssClass="labelpump"></asp:Label></td>
               <td>
                   <asp:Label ID="Label36" runat="server" Text="Time" CssClass="labelpump"></asp:Label></td>
               <td>
                   <asp:Label ID="Label37" runat="server" Text="Qtty. Readout" CssClass="labelpump"></asp:Label></td>
           </tr>
           <tr>
               <td> <asp:Label ID="Label6" runat="server" Text="1" CssClass="lab1"></asp:Label></td>
               <td></td>
               <td></td>
           </tr>
           <tr>
               <td>
                   <asp:Label ID="Label7" runat="server" Text="2" CssClass="lab1"></asp:Label></td>
               <td></td>
               <td></td>
           </tr>
           <tr>
               <td>
                   <asp:Label ID="Label8" runat="server" Text="3" CssClass="lab1"></asp:Label></td>
               <td></td>
               <td></td>
           </tr>
           <tr>
               <td>
                   <asp:Label ID="Label38" runat="server" Text="4" CssClass="lab1"></asp:Label></td>
               <td></td>
               <td></td>
           </tr>
           <tr>
               <td>
                   <asp:Label ID="Label39" runat="server" Text="5" CssClass="lab1"></asp:Label></td>
               <td></td>
               <td></td>
           </tr>
       </table>
   </section><br />
    <asp:Label ID="Label40" runat="server" Text="Remark" CssClass="remark"></asp:Label><br />
   <asp:TextBox ID="TextBox46" runat="server" placeholder="Catatan" TextMode="MultiLine" CssClass="txtremark"></asp:TextBox><br /><br />           
    <table class="tblcargo">
        <tr>
            <td><asp:TextBox ID="TextBox48" runat="server"  placeholder="Cargo Officer" CssClass="co1"></asp:TextBox></td>
            <td><asp:TextBox ID="TextBox47" runat="server" placeholder="Master / Chief Engineer" CssClass="co2"></asp:TextBox></td>
        </tr>
        <tr>
            <td><asp:Label ID="Label41" runat="server" Text="Cargo Officer" CssClass=""></asp:Label></td>
            <td><asp:Label ID="Label42" runat="server" Text="(Master / Chief Engineer)"></asp:Label></td>
        </tr>
    </table>
        <asp:ImageButton ID="ImageButton1" runat="server" CssClass="arrowbtn" OnClick="ImageButton1_Click" ImageUrl="~/assets/img/Arrow.png" Width="50" Height="50" ToolTip="Go to Sample Reciept Page" />
        </section>
</asp:Content>
