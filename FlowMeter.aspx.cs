﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;

namespace MigasLevelUp
{
    public partial class FlowMeter : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                dropdownVessel();
                dropdownNextPort();
            }
        }
        private void dropdownVessel()
        {
            string CS = ConfigurationManager.ConnectionStrings["OilGas"].ConnectionString;
            using (SqlConnection con = new SqlConnection(CS))
            {
                SqlCommand cmd = new SqlCommand("select Id, Vessel from tblVessel", con);
                con.Open();
                DropDownList1.DataSource = cmd.ExecuteReader();
                DropDownList1.DataTextField = "Vessel";
                DropDownList1.DataValueField = "Id";
                DropDownList1.DataBind();
            }
        }
        private void dropdownNextPort()
        {
            string CS = ConfigurationManager.ConnectionStrings["OilGas"].ConnectionString;
            using (SqlConnection con = new SqlConnection(CS))
            {
                SqlCommand cmd = new SqlCommand("select Id, NextPort from tblNextPort", con);
                con.Open();
                DropDownList4.DataSource = cmd.ExecuteReader();
                DropDownList4.DataTextField = "NextPort";
                DropDownList4.DataValueField = "Id";
                DropDownList4.DataBind();
            }

        }
        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/SampleReciept.aspx");
        }
    }
}