﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace MigasWebForm
{
    public partial class Default : System.Web.UI.Page
    {
        DataSet ds = new DataSet();
        string sql = ConfigurationManager.ConnectionStrings["OilGas"].ConnectionString;
        #region "Page Load"
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillGridViewSelling();
                FillGridViewBuying();
                gvbind();
                gvbind1();
                BindSellingDetails();
                BindBuyingDetails();
            }
            /*    if (Session["User"] == null)
            {
                Response.Redirect("Login.aspx");
                if (Request.Cookies["LoginCookies"] != null)
                {
                    Response.Redirect("Login.aspx");
                }
            }
            else
            {
                //lblName.Text = Request.Cookies["LoginCookies"].Value;
            } */
        }
        #endregion
#region "Bind Selling Details"
        private DataTable BindSellingDetails()
        {
            using (SqlConnection con = new SqlConnection(sql))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("select * from SellingList", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                con.Close();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GridView1.DataSource = ds;
                    GridView1.DataBind();
                }
                else
                {
                   
                }
                return ds.Tables[0];
            }
        }
#endregion
        #region "Bind Buying Details"
        private DataTable BindBuyingDetails()
        {
            using (SqlConnection con = new SqlConnection(sql))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("select * from BuyingList", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                con.Close();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GridView2.DataSource = ds;
                    GridView2.DataBind();
                }
                else
                {

                }
                return ds.Tables[0];
            }
        }
        #endregion
        private void FillGridViewBuying()
        {
            using (SqlConnection con = new SqlConnection(sql))
            {
                SqlCommand cmd = new SqlCommand("Select * from BuyingList", con);
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                GridView2.DataSource = rdr;
                GridView2.DataBind();
            }
        }

        private void FillGridViewSelling()
        {

            using (SqlConnection con = new SqlConnection(sql))
            {
                SqlCommand cmd = new SqlCommand("Select * from SellingList", con);
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                GridView1.DataSource = rdr;
                GridView1.DataBind();
            }
        }

        protected void gvbind()
        {
            using (SqlConnection con = new SqlConnection(sql))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from SellingList", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                con.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    GridView1.DataSource = ds;
                    GridView1.DataBind();

                }
                else
                {
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    GridView1.DataSource = ds;
                    GridView1.DataBind();
                    int columncount = GridView1.Rows[0].Cells.Count;
                    GridView1.Rows[0].Cells.Clear();
                    GridView1.Rows[0].Cells.Add(new TableCell());
                    GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                    GridView1.Rows[0].Cells[0].Text = "No Records Found";
                }
            }
        }
        protected void gvbind1()
        {
            using (SqlConnection con = new SqlConnection(sql))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from BuyingList", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                con.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    GridView2.DataSource = ds;
                    GridView2.DataBind();

                }
                else
                {
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    GridView2.DataSource = ds;
                    GridView2.DataBind();
                    int columncount = GridView2.Rows[0].Cells.Count;
                    GridView2.Rows[0].Cells.Clear();
                    GridView2.Rows[0].Cells.Add(new TableCell());
                    GridView2.Rows[0].Cells[0].ColumnSpan = columncount;
                    GridView2.Rows[0].Cells[0].Text = "No Records Found";
                }
            }
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            using (SqlConnection con = new SqlConnection(sql))
            {
                string query = "select [RefNo],[Date],[BuyerName],[Product],[Quantity],[Status] from SellingList where [Id]=N'" + GridView1.Rows[e.NewEditIndex].Cells[1].Text + "'";
                con.Open();
                SqlDataAdapter adp = new SqlDataAdapter(query, con);
                DataSet ds = new DataSet();
                adp.Fill(ds);
                if (con != null)
                {
                    con.Close();
                }
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            gvbind();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int userid = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value.ToString());
            GridViewRow row = (GridViewRow)GridView1.Rows[e.RowIndex];

            TextBox RefNo = (TextBox)row.FindControl("TxtRefNo");
            TextBox Date = (TextBox)row.FindControl("TxtDate");
            TextBox BuyerName = (TextBox)row.FindControl("TxtBuyerName");
            TextBox Product = (TextBox)row.FindControl("TxtProduct");
            TextBox Quantity = (TextBox)row.FindControl("TxtQuantity");
            TextBox Status = (TextBox)row.FindControl("TxtStatus");

            GridView1.EditIndex = -1;

            using (SqlConnection con = new SqlConnection(sql))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("update SellingList set RefNo='" + RefNo.Text + "',Date='" + Date.Text + "',BuyerName='" + BuyerName.Text + "',Product='" + Product.Text + "',Quantity='" + Quantity.Text + "',Status='" + Status.Text + "' where Id='" + userid + "'", con);
                cmd.ExecuteNonQuery();
                con.Close();
                gvbind();
            }
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            FillGridViewSelling();
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = (GridViewRow)GridView1.Rows[e.RowIndex];
            using (SqlConnection con = new SqlConnection(sql))
            {

                if (GridView1.Rows.Count > 0)
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete FROM SellingList where Id='" + Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value.ToString()) + "'", con);
                    cmd.ExecuteNonQuery();
                    con.Close();
                    gvbind();

                }
                else
                {
                    gvbind();
                    //GridView1.Attributes.Add("OnClick", "return confirm('Really wanna delete?');");
                }
            }
        }
       
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            AddConfirmDelete((GridView)sender, e);
        }
        public static void AddConfirmDelete(GridView gv, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                foreach (DataControlField dcf in gv.Columns)
                {
                    if (dcf.ToString() == "CommandField")
                    {
                        if (((CommandField)dcf).ShowDeleteButton == true)
                        {
                            e.Row.Cells[gv.Columns.IndexOf(dcf)].Attributes.Add("onclick", "return confirm(\"Are you sure?\")");
                        }
                    }
                }
            }
        }
        protected void GridView2_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView2.EditIndex = -1;
            gvbind1();
        }

        protected void GridView2_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView2.EditIndex = e.NewEditIndex;
            string CS = ConfigurationManager.ConnectionStrings["OilGas"].ConnectionString;
            using (SqlConnection con = new SqlConnection(CS))
            {
                string sql = "select [RefNo],[Date],[BuyerName],[Product],[Quantity],[Status] from BuyingList where [Id]=N'" + GridView2.Rows[e.NewEditIndex].Cells[1].Text + "'";
                con.Open();
                SqlDataAdapter adp = new SqlDataAdapter(sql, con);
                DataSet ds = new DataSet();
                adp.Fill(ds);
                if (con != null)
                {
                    con.Close();
                }
                GridView2.DataSource = ds;
                GridView2.DataBind();
            }
            gvbind1();
        }

        protected void GridView2_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int userid = Convert.ToInt32(GridView2.DataKeys[e.RowIndex].Value.ToString());
            GridViewRow row = (GridViewRow)GridView2.Rows[e.RowIndex];

            TextBox RefNo = (TextBox)row.FindControl("TxtRefNo1");
            TextBox Date = (TextBox)row.FindControl("TxtDate1");
            TextBox BuyerName = (TextBox)row.FindControl("TxtBuyerName1");
            TextBox Product = (TextBox)row.FindControl("TxtProduct1");
            TextBox Quantity = (TextBox)row.FindControl("TxtQuantity1");
            TextBox Status = (TextBox)row.FindControl("TxtStatus1");

            GridView2.EditIndex = -1;

            string CS = ConfigurationManager.ConnectionStrings["OilGas"].ConnectionString;
            using (SqlConnection con = new SqlConnection(CS))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("update BuyingList set RefNo='" + RefNo.Text + "',Date='" + Date.Text + "',BuyerName='" + BuyerName.Text + "',Product='" + Product.Text + "',Quantity='" + Quantity.Text + "',Status='" + Status.Text + "' where Id='" + userid + "'", con);
                cmd.ExecuteNonQuery();
                con.Close();
                gvbind1();
            }
        }

        protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = (GridViewRow)GridView2.Rows[e.RowIndex];
            using (SqlConnection con = new SqlConnection(sql))
            {
                if (GridView2.Rows.Count <= 0) //  (GridView2.Rows.Count > 0)
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete FROM BuyingList where Id='" + Convert.ToInt32(GridView2.DataKeys[e.RowIndex].Value.ToString()) + "'", con);
                    cmd.ExecuteNonQuery();
                    con.Close();
                    gvbind1();
                }
                else
                {
                    GridView2.Attributes.Add("OnClick", "return confirm('Really wanna delete?');");
                }
            }
        }
        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            AddConfirmDelete((GridView)sender, e);

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(sql))
            {
                if (TextBoxSearch.Text != null && TextBoxSearch.Text != "")
                {
                    string find = "select * from SellingList where(RefNo like '%' + @RefNo + '%' or Date like '%' + @Date + '%' or BuyerName like '%' + @BuyerName + '%' or Product like '%' + @Product + '%' or Quantity like '%' + @Quantity + '%' or Status like '%' + @Status + '%')";
                    SqlCommand cmd = new SqlCommand(find, con);
                    cmd.Parameters.Add("@RefNo", SqlDbType.NVarChar).Value = TextBoxSearch.Text;
                    cmd.Parameters.Add("@Date", SqlDbType.NVarChar).Value = TextBoxSearch.Text;
                    cmd.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = TextBoxSearch.Text;
                    cmd.Parameters.Add("@Product", SqlDbType.NVarChar).Value = TextBoxSearch.Text;
                    cmd.Parameters.Add("@Quantity", SqlDbType.NVarChar).Value = TextBoxSearch.Text;
                    cmd.Parameters.Add("@Status", SqlDbType.NVarChar).Value = TextBoxSearch.Text;

                    con.Open();
                    cmd.ExecuteNonQuery();
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds, "RefNo");
                    da.Fill(ds, "Date");
                    da.Fill(ds, "BuyerName");
                    da.Fill(ds, "Product");
                    da.Fill(ds, "Quantity");
                    da.Fill(ds, "Status");
                    GridView1.DataSource = ds;
                    GridView1.DataBind();

                    con.Close();
                }
                else
                {
                    GridView1.DataSource = null;
                    GridView1.DataBind();
                }
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(sql))
            {
                if (TextBoxSearch1.Text != null && TextBoxSearch1.Text != "")
                {
                    string find = "select * from BuyingList where(RefNo like '%' + @RefNo + '%' or Date like '%' + @Date + '%' or BuyerName like '%' + @BuyerName + '%' or Product like '%' + @Product + '%' or Quantity like '%' + @Quantity + '%' or Status like '%' + @Status + '%')";
                    SqlCommand cmd = new SqlCommand(find, con);
                    cmd.Parameters.Add("@RefNo", SqlDbType.NVarChar).Value = TextBoxSearch1.Text;
                    cmd.Parameters.Add("@Date", SqlDbType.NVarChar).Value = TextBoxSearch1.Text;
                    cmd.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = TextBoxSearch1.Text;
                    cmd.Parameters.Add("@Product", SqlDbType.NVarChar).Value = TextBoxSearch1.Text;
                    cmd.Parameters.Add("@Quantity", SqlDbType.NVarChar).Value = TextBoxSearch1.Text;
                    cmd.Parameters.Add("@Status", SqlDbType.NVarChar).Value = TextBoxSearch1.Text;

                    con.Open();
                    cmd.ExecuteNonQuery();
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds, "RefNo");
                    da.Fill(ds, "Date");
                    da.Fill(ds, "BuyerName");
                    da.Fill(ds, "Product");
                    da.Fill(ds, "Quantity");
                    da.Fill(ds, "Status");
                    GridView2.DataSource = ds;
                    GridView2.DataBind();

                    con.Close();
                }
                else
                {
                    GridView2.DataSource = null;
                    GridView2.DataBind();
                }
            }
        }

        //maintain the SortDirection (Ascending or Descending) in ViewState
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirstate"] == null)
                {
                    ViewState["dirstate"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirstate"];
            }
            set
            {
                ViewState["dirstate"] = value;
            }
        }
        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            string sortingDirection = string.Empty;
            if (direction == SortDirection.Ascending)
            {
                direction = SortDirection.Descending;
                sortingDirection = "Desc";
            }
            else
            {
                direction = SortDirection.Ascending;
                sortingDirection = "ASC";
            }
            DataView sortedView = new DataView(BindSellingDetails());
            sortedView.Sort = e.SortExpression + " " + sortingDirection;
            Session["SortedView"] = sortedView;
            GridView1.DataSource = sortedView;
            GridView1.DataBind();
           
        }

        protected void GridView2_Sorting(object sender, GridViewSortEventArgs e)
        {
            string sortingDirection = string.Empty;
            if (direction == SortDirection.Ascending)
            {
                direction = SortDirection.Descending;
                sortingDirection = "Desc";
            }
            else
            {
                direction = SortDirection.Ascending;
                sortingDirection = "ASC";
            }
            DataView sortedView = new DataView(BindBuyingDetails());
            sortedView.Sort = e.SortExpression + " " + sortingDirection;
            Session["SortedView"] = sortedView;
            GridView2.DataSource = sortedView;
            GridView2.DataBind();
        }

        protected void GridView1_DataBound(object sender, EventArgs e)
        {
            for (int index = 0; index < GridView1.HeaderRow.Cells.Count; index++)
            {
                if (index == 0 || index == 1 || index == 2 || index == 3 || index == 4 || index == 5 || index == 6)
                {
                    var celll = GridView1.HeaderRow.Cells[index];
                    var headerText = GridView1.HeaderRow.Cells[index].Text;

                    Image image = new Image { ImageUrl = "../assets/img/UpDown50x50.png" };
                    image.Attributes.Add("style", "margin-left:2em;margin-right:0em;width:25px;height:25px;");
                    image.Visible = true;
                    celll.Controls.Add(image);                   
                }
            }
        }

        protected void GridView2_DataBound(object sender, EventArgs e)
        {
            for (int index1 = 0; index1 < GridView2.HeaderRow.Cells.Count; index1++)
            {
                if (index1 == 0 || index1 == 1 || index1 == 2 || index1 == 3 || index1 == 4 || index1 == 5 || index1 == 6)
                {
                    var celll = GridView2.HeaderRow.Cells[index1];
                    var headerText = GridView2.HeaderRow.Cells[index1].Text;

                    Image image1 = new Image { ImageUrl = "../assets/img/UpDown50x50.png" };
                    image1.Attributes.Add("style", "margin-left:2em;margin-right:0em;width:25px;height:25px;");
                    image1.Visible = true;
                    celll.Controls.Add(image1);
                }
            }
        }
    }
}