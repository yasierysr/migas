﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeBehind="NewBuying.aspx.cs" Inherits="MigasLevelUp.NewBuying" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="assets/js/jquery-1.10.2.js"></script>
    <script src="assets/js/Jquery.js"></script>
      <script type="text/javascript">
          $(document).ready(function () {
              $('.calender-show1').hide();
              $('.calender-show2').hide();
              $('.calender-show3').hide();
          })
          function BtnCalender1() {
              $('.calender-show1').fadeToggle("slow");
              $('.calender-show2').hide();
              $('.calender-show3').hide();
              return false;
          }
          function BtnCalender2() {
              $('.calender-show2').fadeToggle("slow");
              $('.calender-show1').hide();
              $('.calender-show3').hide();
              return false;
          }
          function BtnCalender3() {
              $('.calender-show3').fadeToggle("slow");
              $('.calender-show2').hide();
              $('.calender-show1').hide();
              return false;
          }
    </script>
    <style>
        .CalenderSellingCommencedPumping {
            position: absolute;
            margin-left:35.5em;
            margin-top:0.1em;
        }
        .CalenderSellingCompletedPumping {
            position: absolute;
            margin-left:35.5em;
            margin-top:0.1em;
        }
        .CalenderTanggal {
            position: absolute;
            margin-left:35.5em;
            margin-top:0.1em;
        }
        .rhyhorn {
            position:absolute;
            margin-top:47.4em;
            margin-left:9.8em;
            z-index:1;
        }
        .rhydon {
            position:absolute;
            margin-top:55.3em;
            margin-left:9.8em;
            z-index:1;
        }
        .unicorn {
            position:absolute;
            margin-top:24em;
            margin-left:49.4em;
            z-index:1;
        }
        .buyingsection {
            padding-top:8em;
        }
        .arrowbtn {
            position:absolute;
           margin-left:73em;
           
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <!-- CONTENT WRAPPER END -->
    <section class="rhyhorn">
    <asp:Calendar ID="Calendar1" runat="server" CssClass="calender-show1" OnSelectionChanged="Calendar1_SelectionChanged"
                        BackColor="White" BorderColor="Black" DayNameFormat="Full" Font-Names="Times New Roman" Font-Size="10pt"
                        ForeColor="Black" Height="220px" NextPrevFormat="FullMonth" TitleFormat="MonthYear" Width="520px" NextPrevStyle-CssClass="electabuzz">
                        <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="10pt" ForeColor="#333333" Height="10pt" CssClass="charizard" />
                        <DayStyle Width="14%" />
                        <NextPrevStyle Font-Size="12pt" ForeColor="White" CssClass="ash" />
                        <OtherMonthDayStyle ForeColor="#999999" />
                        <SelectedDayStyle BackColor="#CC3333" ForeColor="White" />
                        <SelectorStyle BackColor="#CCCCCC" Font-Bold="True" Font-Names="Verdana" Font-Size="48pt" ForeColor="#333333" Width="1%" />
                        <TitleStyle BackColor="dodgerblue" Font-Bold="True" Font-Size="15pt" ForeColor="White" Height="15pt" CssClass="magmar" />
                        <TodayDayStyle BackColor="dodgerblue" ForeColor="White" />
                    </asp:Calendar>
        </section>
    <section class="rhydon">
    <asp:Calendar ID="Calendar2" runat="server" CssClass="calender-show2" OnSelectionChanged="Calendar2_SelectionChanged"
                        BackColor="White" BorderColor="Black" DayNameFormat="Full" Font-Names="Times New Roman" Font-Size="10pt"
                        ForeColor="Black" Height="220px" NextPrevFormat="FullMonth" TitleFormat="MonthYear" Width="520px" NextPrevStyle-CssClass="electabuzz">
                        <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="10pt" ForeColor="#333333" Height="10pt" CssClass="charizard" />
                        <DayStyle Width="14%" />
                        <NextPrevStyle Font-Size="12pt" ForeColor="White" CssClass="ash" />
                        <OtherMonthDayStyle ForeColor="#999999" />
                        <SelectedDayStyle BackColor="#CC3333" ForeColor="White" />
                        <SelectorStyle BackColor="#CCCCCC" Font-Bold="True" Font-Names="Verdana" Font-Size="48pt" ForeColor="#333333" Width="1%" />
                        <TitleStyle BackColor="dodgerblue" Font-Bold="True" Font-Size="15pt" ForeColor="White" Height="15pt" CssClass="magmar" />
                        <TodayDayStyle BackColor="dodgerblue" ForeColor="White" />
                   </asp:Calendar>
        </section>
    <section class="unicorn">
    <asp:Calendar ID="Calendar3" runat="server" CssClass="calender-show3" OnSelectionChanged="Calendar3_SelectionChanged"
                        BackColor="White" BorderColor="Black" DayNameFormat="Full" Font-Names="Times New Roman" Font-Size="10pt"
                        ForeColor="Black" Height="220px" NextPrevFormat="FullMonth" TitleFormat="MonthYear" Width="520px" NextPrevStyle-CssClass="electabuzz">
                        <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="10pt" ForeColor="#333333" Height="10pt" CssClass="charizard" />
                        <DayStyle Width="14%" />
                        <NextPrevStyle Font-Size="12pt" ForeColor="White" CssClass="ash" />
                        <OtherMonthDayStyle ForeColor="#999999" />
                        <SelectedDayStyle BackColor="#CC3333" ForeColor="White" />
                        <SelectorStyle BackColor="#CCCCCC" Font-Bold="True" Font-Names="Verdana" Font-Size="48pt" ForeColor="#333333" Width="1%" />
                        <TitleStyle BackColor="dodgerblue" Font-Bold="True" Font-Size="15pt" ForeColor="White" Height="15pt" CssClass="magmar" />
                        <TodayDayStyle BackColor="dodgerblue" ForeColor="White" />
                    </asp:Calendar>
        </section>
    <section class="buyingsection">
    <div class="content-wrapper">
        <div class="container">
            <div class="row pad-botm">
                <div class="col-md-12">
                    <h4 class="header-line">New Buying</h4>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#BunkerDelivery" class="text-center-block text-bold text-decoration-none">
                                    BUNKER DELIVERY RECEIPT
                                </a>
                            </h4>
                        </div>
                        <div id="BunkerDelivery" class="panel-body panel-collapse in">
                            <form role="form">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group text-center-block">
                                        <label class="text-bold double-line-bottom">128 / JE-BDR / CUST/ VII /</label><asp:Label ID="LabelMonth" runat="server"></asp:Label><b class="bbold">/</b><asp:Label ID="Labelyears" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="DikirimDi">Delivered At</label>
                                        <div class="text-bottom-label">Dikirim Di</div>
                                         <asp:DropDownList ID="DropDownList1" runat="server" Class="form-control">
                                            <asp:ListItem Text="Silahkan Pilih" Value="-1"></asp:ListItem>
                                            <asp:ListItem Text="Indonesia"></asp:ListItem>
                                            <asp:ListItem Text="Malaysia"></asp:ListItem>
                                            <asp:ListItem Text="China"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group">
                                        <label for="DikirimOleh">Delivered By</label>
                                        <div class="text-bottom-label">Dikirim Oleh</div>
                                        <asp:TextBox ID="TextBox1" runat="server" Class="form-control" placeholder="Dikirim Oleh"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        <label for="JenisProduk">Grade</label>
                                        <div class="text-bottom-label">Jenis Produk</div>
                                      
                                        <asp:DropDownList ID="DropDownList2" runat="server" Class="form-control">
                                            <asp:ListItem Text="Silahkan Pilih" Value="-1"></asp:ListItem>
                                            <asp:ListItem Text="Pertamina"></asp:ListItem>
                                            <asp:ListItem Text="Solar"></asp:ListItem>
                                            <asp:ListItem Text="Bio Gas"></asp:ListItem>
                                            <asp:ListItem Text="Pertamax"></asp:ListItem>
                                            <asp:ListItem Text="Pertalite"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group">
                                        <label for="MulaiPemompaan">Commenced Pumping</label>
                                        <div class="text-bottom-label">Mulai Pemompaan</div>
                                        <asp:ImageButton ID="ImageButtonMulaiPemompaan" runat="server" ImageUrl="~/assets/img/calendar.png" CssClass="CalenderSellingCommencedPumping" Width="25" Height="30" OnClientClick="javascript:return BtnCalender1();"  /> 
                                        <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" placeholder="Mulai Pemompaan"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        <label for="SelesaiPemompaan">Completed Pumping</label>
                                        <div class="text-bottom-label">Selesai Pemompaan</div>
                                        <asp:ImageButton ID="ImageButtonSelesaiPemompaan" runat="server" ImageUrl="~/assets/img/calendar.png" CssClass="CalenderSellingCompletedPumping" Width="25" Height="30" OnClientClick="javascript:return BtnCalender2();" />
                                        <asp:TextBox ID="TextBox3" runat="server" CssClass="form-control" placeholder="Selesai Pemompaan"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="Date">Date</label>
                                        <div class="text-bottom-label">Tanggal</div>
                                        <asp:ImageButton ID="ImageButtonTanggal" runat="server" ImageUrl="~/assets/img/calendar.png" CssClass="CalenderTanggal" Width="25" Height="30" OnClientClick="javascript:return BtnCalender3();" />
                                        <asp:TextBox ID="TextBox4" runat="server" CssClass="form-control" placeholder="Tanggal"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        <label for="Vessel's">Vessel's Name</label>
                                        <div class="text-bottom-label">Nama Kapal</div>
                                      
                                        <asp:DropDownList ID="DropDownList3" runat="server" CssClass="form-control">
                                            <asp:ListItem Text="Silahkan Pilih" Value="-1"></asp:ListItem>
                                            <asp:ListItem Text="Tanker"></asp:ListItem>
                                            <asp:ListItem Text="Persiar"></asp:ListItem>
                                            <asp:ListItem Text="Ferry"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group">
                                        <label for="GT">GT</label>
                                        <div class="text-bottom-label">Berat Kotor</div>
                                      
                                         <asp:TextBox ID="TextBox5" runat="server" CssClass="form-control" placeholder="Berat Kotor"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        <label for="NextPort">Next Port</label>
                                        <div class="text-bottom-label">Pel. Lanjutan</div>
                                      
                                        <asp:DropDownList ID="DropDownList4" runat="server" CssClass="form-control">
                                            <asp:ListItem Text="Silahkan Pilih" Value="-1"></asp:ListItem>
                                            <asp:ListItem Text="Tanjung priuk"></asp:ListItem>
                                            <asp:ListItem Text="Batam"></asp:ListItem>
                                            <asp:ListItem Text="Bakahuni"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group">
                                        <label for="E.T.D">E.T.D</label>
                                        <div class="text-bottom-label">Perkiraan Waktu Keberangkatan</div>
                                      
                                        <asp:TextBox ID="TextBox6" runat="server" CssClass="form-control" placeholder="Perkiraan Waktu Keberangkatan"></asp:TextBox>
                                    </div>
                                </div>
                            </form>
                            <hr/>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                             <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#ProductSupplied" class="text-center-block text-bold text-decoration-none">
                                    PRODUCT SUPPLIED - PRODUK YANG DIKIRIM
                                </a>
                            </h4>                            
                        </div>
                        <div id="ProductSupplied" class="panel-body panel-collapse in">
                            
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border">FUEL CHARACTERISTICS - KARAKTERISTIK BAHAN BAKAR</legend>
                                        <div class="form-group">
                                            <label for="Visc.cST">Visc. cST @40°C / 50°C</label>
                                            <div class="text-bottom-label">ASTM D445 / ISO 3104</div>
                                         
                                             <asp:TextBox ID="TextBox7" runat="server" CssClass="form-control" placeholder="ASTM D445 / ISO 3104"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label for="Density">Density @ 15°C</label>
                                            <div class="text-bottom-label">ASTM D1298-99</div>
                                      
                                            <asp:TextBox ID="TextBox8" runat="server" CssClass="form-control" placeholder="ASTM D1298-99"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label for="Flashpoint">Flashpoint °C</label>
                                            <div class="text-bottom-label">ASTM D93 / ISO 2719</div>
                                      
                                            <asp:TextBox ID="TextBox9" runat="server" CssClass="form-control" placeholder="ASTM D93 / ISO 2719"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label for="Sulphur">Sulphur wt%</label>
                                            <div class="text-bottom-label">ASTM D4294-03</div>
                                      
                                            <asp:TextBox ID="TextBox10" runat="server" CssClass="form-control" placeholder="ASTM D4294-03"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label for="WaterContent">Water Content % Vol.</label>
                                            <div class="text-bottom-label">ASTM D95 / ISO 3733</div>
                                      
                                            <asp:TextBox ID="TextBox11" runat="server" CssClass="form-control" placeholder="ASTM D95 / ISO 3733"></asp:TextBox>
                                        </div>
                                    </fieldset>                                    
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border">Quantity - Jumlah</legend>
                                        <div class="form-group">
                                            <label for="GrossVol">Gross Vol. Litres</label>
                                      
                                            <asp:TextBox ID="TextBox12" runat="server" CssClass="form-control" placeholder="Gross Vol. Litres"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label for="NetVol">Net Vol. Litres</label>
                                      
                                            <asp:TextBox ID="TextBox13" runat="server" CssClass="form-control" placeholder="Net Vol. Litres"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label for="NetMetric">Net Metric Tons</label>
                                      
                                            <asp:TextBox ID="TextBox14" runat="server" CssClass="form-control" placeholder="Net Metric Tons"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label for="NetLong">Net Long Tons</label>
                                      
                                            <asp:TextBox ID="TextBox15" runat="server" CssClass="form-control" placeholder="Net Long Tons"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label for="NetBarrel">Net Barrel AT 60°F</label>
                                      
                                            <asp:TextBox ID="TextBox16" runat="server" CssClass="form-control" placeholder="Net Barrel AT 60°F"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label for="V.C.F">V.C.F</label>
                                            <div class="text-bottom-label">ASTM tab. 54B</div>
                                      
                                            <asp:TextBox ID="TextBox17" runat="server" CssClass="form-control" placeholder="ASTM tab. 54B"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label for="W.C.F">W.C.F</label>
                                            <div class="text-bottom-label">ASTM tab. 56</div>
                                      
                                             <asp:TextBox ID="TextBox18" runat="server" CssClass="form-control" placeholder="ASTM tab. 56"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label for="Temperature°C">Temperature °C</label>
                                      
                                            <asp:TextBox ID="TextBox19" runat="server" CssClass="form-control" placeholder="Temperature °C"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label for="Table52">Table 52</label>
                                      
                                            <asp:TextBox ID="TextBox20" runat="server" CssClass="form-control" placeholder="Table 52"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label for="Table1">Table 1</label>
                                            <div class="text-bottom-label">MT / LT</div>
                                      
                                             <asp:TextBox ID="TextBox21" runat="server" CssClass="form-control" placeholder="MT / LT"></asp:TextBox>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border">SUPPLIER'S CONFIRMATION - KONFIRMASI PEMASOK</legend>
                                        <div class="form-group">
                                            <label>We confirm that the above product was delivered and that the quantities were correct.</label>
                                            <div class="text-bottom-label">Kami konfirmasikan bahwa produk diatas telah dikirim dan jumlahnya telah disetujui.</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="For">For</label>
                                            <div class="text-bottom-label">Dari</div>
                                      
                                             <asp:TextBox ID="TextBox22" runat="server" CssClass="form-control" placeholder="Dari"></asp:TextBox>
                                            <div class="text-bottom-label">(Company's Name)</div>
                                        </div>
                                        <div class="form-group" style="padding:50px;">
                                        </div>
                                        <div class="form-group">
                                      
                                            <asp:TextBox ID="TextBox23" runat="server" CssClass="form-control" placeholder="Cargo Officer"></asp:TextBox>
                                            <label for="CargoOfficer">(Cargo Officer)</label>
                                        </div>
                                    </fieldset>                                    
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border">
                                            MASTER'S / CHIEF ENGINEER'S - PENGAKUAN
                                            <br />
                                            ACKNOWLEDGEMENT - KAPTEN / KKM
                                        </legend>
                                        <div class="form-group">
                                            <label>We acknowledge receipt of the above product and confirm that samples were taken, sealed and numbered as follow.</label>
                                            <div class="text-bottom-label">Kami mengakui adanya penerimaan produk diatas dan mengkonfirmasikan telah diambil sample, disegel dengan nomor sebagai berikut.</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Vessel">Vessel</label>
                                            <div class="text-bottom-label">Kapal</div>
                                      
                                            <asp:DropDownList ID="DropDownList5" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="Silahkan Pilih" Value="-1"></asp:ListItem>
                                                <asp:ListItem Text="Tanker"></asp:ListItem>
                                                <asp:ListItem Text="Persiar"></asp:ListItem>
                                                <asp:ListItem Text="Ferry"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group">
                                            <label for="BunkerTanker">Bunker Tanker</label>
                                      
                                            <asp:TextBox ID="TextBox24" runat="server" CssClass="form-control" placeholder="Bunker Tanker"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label for="Surveyor">Surveyor</label>
                                      
                                            <asp:TextBox ID="TextBox25" runat="server" CssClass="form-control" placeholder="Surveyor"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label for="WaterContent">Acknowledged By</label>
                                            <div class="text-bottom-label">Diakui Oleh</div>
                                            <div style="padding:50px;"></div>
                                        </div>
                                        <div class="form-group">
                                      
                                             <asp:TextBox ID="TextBox26" runat="server" CssClass="form-control" placeholder="Master / Chief Engineer"></asp:TextBox>
                                            <label for="ChiefEngineer">(Master / Chief Engineer)</label>
                                        </div>
                                    </fieldset>                                    
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="REMAKS">Remaks</label>
                                        <div class="text-bottom-label">Catatan</div>
                                        <asp:TextBox ID="TextBox27" runat="server" CssClass="form-control" placeholder="Catatan" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                    <asp:ImageButton ID="ImageButton1" runat="server" CssClass="arrowbtn" OnClick="ImageButton1_Click" ImageUrl="~/assets/img/Arrow.png" Width="50" Height="50" ToolTip="Go to Flow Meter Page" />
                                    <asp:Button ID="Button1" runat="server" Text="SIMPAN" CssClass="btn btn-info center-block" Width="150" Height="45" OnClick="Button1_Click" />
                                </div>
                            
                            <hr/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        </section>
<!-- CONTENT WRAPPER END -->

</asp:Content>
