﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace MigasLevelUp
{
    public partial class NewPlanning : System.Web.UI.Page
    {
        string CS = ConfigurationManager.ConnectionStrings["OilGas"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            SqlConnection sqlcon = new SqlConnection(CS);
            sqlcon.Open();
            string query = "Insert into Planning (JenisBBM, PosTarif, VolumeKL, Harga, Asal, Tujuan, Jetti, Penyimpanan, AlatAngkut, Ket) values (@JenisBBM, @PosTarif, @VolumeKL, @Harga, @Asal, @Tujuan, @Jetti, @Penyimpanan, @AlatAngkut, @Ket)";
            SqlCommand cmd = new SqlCommand(query, sqlcon);
            cmd.Parameters.Add(new SqlParameter("@JenisBBM", DropDownList1.SelectedItem.ToString()));
            cmd.Parameters.Add(new SqlParameter("@PosTarif", TextBox2.Text));
            cmd.Parameters.Add(new SqlParameter("@VolumeKL", DropDownList5.SelectedItem.ToString()));
            cmd.Parameters.Add(new SqlParameter("@Harga", TextBox3.Text));
            cmd.Parameters.Add(new SqlParameter("@Asal", DropDownList2.SelectedItem.ToString()));
            cmd.Parameters.Add(new SqlParameter("@Tujuan", DropDownList3.SelectedItem.ToString()));
            cmd.Parameters.Add(new SqlParameter("@Jetti", DropDownList5.SelectedItem.ToString()));
            cmd.Parameters.Add(new SqlParameter("@Penyimpanan", DropDownList4.SelectedItem.ToString()));
            cmd.Parameters.Add(new SqlParameter("@AlatAngkut", DropDownList6.SelectedItem.ToString()));
            cmd.Parameters.Add(new SqlParameter("@Ket", TextBox4.Text));
           

            cmd.ExecuteNonQuery();
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Inserted Successfully')", true);
            Response.Redirect(Request.RawUrl);
        }
    }
}