﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeBehind="NewPlanning.aspx.cs" Inherits="MigasLevelUp.NewPlanning" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .planB {
            padding-top:11em;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <!-- CONTENT WRAPPER END -->
    <section class="planB">
    <div class="content-wrapper">
        <div class="container">
            <div class="row pad-botm">
                <div class="col-md-12">
                    <h4 class="header-line">New Planning</h4>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#ClickMe" class="text-center text-bold text-decoration-none"></a>
                            </h4>
                        </div>
                        <div id="ClickMe" class="panel-body panel-collapse in">
                            <form role="form">
                                <div class="col-md-4 col-sm-4 col-xs-12"></div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="NamaProduk">Nama Produk</label>
                                        <%--<select class="form-control" id="NamaProduk">
                                            <option>Silahkan Pilih</option>
                                            <option>Minyak Solar 10ppm</option>
                                            <option>Minyak Diesel</option>
                                            <option>Minyak Bakar</option>
                                            <option>Gasoline RON 95</option>
                                        </select>--%>
                                        <asp:DropDownList ID="DropDownList1" runat="server" Class="form-control">
                                            <asp:ListItem Text="Silahkan Pilih" Value="-1"></asp:ListItem>
                                            <asp:ListItem Text="Minyak Solar 10ppm"></asp:ListItem>
                                            <asp:ListItem Text="Minyak Diesel"></asp:ListItem>
                                            <asp:ListItem Text="Minyak Bakar"></asp:ListItem>
                                            <asp:ListItem Text="Gasoline RON 95"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group">
                                        <label for="PosTarif">Post Tarif /HS</label>
                                        <%--<input type="text" class="form-control" id="PosTarif" placeholder="Post Tarif /HS" />--%>
                                        <asp:TextBox ID="TextBox1" runat="server" Class="form-control" placeholder="Post Tarif /HS"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        <label for="Volume">Volume (KL)</label>
                                        <%--<input type="text" class="form-control" id="Volume" placeholder="Volume (KL)" />--%>
                                        <asp:TextBox ID="TextBox2" runat="server" Class="form-control" placeholder="Volume (KL)"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        <label for="Harga">Harga US$ /KL</label>
                                        <%--<input type="text" class="form-control" id="Harga" placeholder="Harga US$ /KL" />--%>
                                        <asp:TextBox ID="TextBox3" runat="server" Class="form-control" placeholder="Harga US$ /KL"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        <label for="Asal">Asal</label>
                                        <%--<select class="form-control" id="Asal">
                                            <option>Silahkan Pilih</option>
                                            <option>Malaysia</option>
                                            <option>Singapura</option>
                                            <option>Thailand</option>
                                        </select>--%>
                                        <asp:DropDownList ID="DropDownList2" runat="server" Class="form-control">
                                            <asp:ListItem Text="Silahkan Pilih" Value="-1"></asp:ListItem>
                                            <asp:ListItem Text="Malaysia"></asp:ListItem>
                                            <asp:ListItem Text="Singapura"></asp:ListItem>
                                            <asp:ListItem Text="Thailand"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group">
                                        <label for="Tujuan">Tujuan</label>
                                        <%--<select class="form-control" id="Tujuan">
                                            <option>Silahkan Pilih</option>
                                            <option>Jakarta, Indonesia</option>
                                            <option>Batam, Indonesia</option>
                                        </select>--%>
                                        <asp:DropDownList ID="DropDownList3" runat="server" Class="form-control">
                                            <asp:ListItem Text="Silahkan Pilih" Value="-1"></asp:ListItem>
                                            <asp:ListItem Text="Jakarta, Indonesia"></asp:ListItem>
                                            <asp:ListItem Text="Batam, Indonesia"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group">
                                        <label for="Jetti">Jetti</label>
                                        <asp:DropDownList ID="DropDownList5" runat="server" CssClass="form-control">
                                            <asp:ListItem Text="Silahkan Pilih" Value="-1"></asp:ListItem>
                                            <asp:ListItem Text="PT. Toyota Motor Manufacturing Indonesia"></asp:ListItem>
                                            <asp:ListItem Text="PT. Jagad Energy"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group">
                                        <label for="Penyimpanan">Penyimpanan</label>
                                        <asp:DropDownList ID="DropDownList4" runat="server" CssClass="form-control">
                                            <asp:ListItem Text="Silahkan Pilih" Value="-1"></asp:ListItem>
                                            <asp:ListItem Text="PT. Toyota Motor Manufacturing Indonesia"></asp:ListItem>
                                            <asp:ListItem Text="PT. Jagad Energy"></asp:ListItem>
                                            
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group">
                                        <label for="Alat Angkut">Alat Angkut</label>
                                        <asp:DropDownList ID="DropDownList6" runat="server" CssClass="form-control">
                                            <asp:ListItem Text="Silahkan Pilih" Value="-1"></asp:ListItem>
                                            <asp:ListItem Text="Kapal Tanker"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group">
                                        <label for="Keterangan">Keterangan</label>
                                       <%-- <input type="text" class="form-control" id="Keterangan" placeholder="Keterangan" />--%>
                                        <asp:TextBox ID="TextBox4" runat="server" Class="form-control" placeholder="Keterangan" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                    <%--<button type="submit" class="btn btn-info center-block">SIMPAN</button>--%>
                                    <asp:Button ID="Button1" runat="server" Text="SIMPAN" CssClass="btn btn-info center-block" Width="150" Height="45" OnClick="Button1_Click" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        </section>
    <!-- CONTENT WRAPPER END -->
</asp:Content>
