﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace MigasLevelUp
{
    public partial class NewSelling : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                dropdownDeliveredAt();
                dropdownGrade();
                dropdownVessel();
                dropdown5Vessel();
                dropdownNextPort();
            }
        }
        private void dropdownDeliveredAt()
        {
            string CS = ConfigurationManager.ConnectionStrings["OilGas"].ConnectionString;
            using (SqlConnection con = new SqlConnection(CS))
            {
                SqlCommand cmd = new SqlCommand("select Id, DeliveredAt from tblDeliveredAt", con);
                con.Open();
                DropDownList1.DataSource = cmd.ExecuteReader();
                DropDownList1.DataTextField = "DeliveredAt";
                DropDownList1.DataValueField = "Id";
                DropDownList1.DataBind();
            }
        }
        private void dropdownGrade()
        {
            string CS = ConfigurationManager.ConnectionStrings["OilGas"].ConnectionString;
            using (SqlConnection con = new SqlConnection(CS))
            {
                SqlCommand cmd = new SqlCommand("select Id, Grade from tblGrade", con);
                con.Open();
                DropDownList2.DataSource = cmd.ExecuteReader();
                DropDownList2.DataTextField = "Grade";
                DropDownList2.DataValueField = "Id";
                DropDownList2.DataBind();
            }
        }
        private void dropdownVessel()
        {
            string CS = ConfigurationManager.ConnectionStrings["OilGas"].ConnectionString;
            using (SqlConnection con = new SqlConnection(CS))
            {
                SqlCommand cmd = new SqlCommand("select Id, Vessel from tblVessel", con);
                con.Open();
                DropDownList3.DataSource = cmd.ExecuteReader();
                DropDownList3.DataTextField = "Vessel";
                DropDownList3.DataValueField = "Id";
                DropDownList3.DataBind();
            }
        }
        private void dropdown5Vessel()
        {
            string CS = ConfigurationManager.ConnectionStrings["OilGas"].ConnectionString;
            using (SqlConnection con = new SqlConnection(CS))
            {
                SqlCommand cmd = new SqlCommand("select Id, Vessel from tblVessel", con);
                con.Open();
                DropDownList5.DataSource = cmd.ExecuteReader();
                DropDownList5.DataTextField = "Vessel";
                DropDownList5.DataValueField = "Id";
                DropDownList5.DataBind();
            }
        }
        private void dropdownNextPort()
        {
            string CS = ConfigurationManager.ConnectionStrings["OilGas"].ConnectionString;
            using (SqlConnection con = new SqlConnection(CS))
            {
                SqlCommand cmd = new SqlCommand("select Id, NextPort from tblNextPort", con);
                con.Open();
                DropDownList4.DataSource = cmd.ExecuteReader();
                DropDownList4.DataTextField = "NextPort";
                DropDownList4.DataValueField = "Id";
                DropDownList4.DataBind();
            }

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            string CS = ConfigurationManager.ConnectionStrings["OilGas"].ConnectionString;
            using (SqlConnection con = new SqlConnection(CS))
            {
                SqlCommand cmd = new SqlCommand("spSelling", con);
                cmd.CommandType = CommandType.StoredProcedure;


                SqlParameter satu = new SqlParameter("@DeliveredAt", DropDownList1.SelectedItem.ToString());
                SqlParameter dua = new SqlParameter("@DeliveredBy", TextBox1.Text.ToString());
                SqlParameter tiga = new SqlParameter("@Grade", DropDownList2.SelectedItem.ToString());
                SqlParameter empat = new SqlParameter("@CommencedPumping", TextBox2.Text.ToString());
                SqlParameter lima = new SqlParameter("@CompletedPumping", TextBox3.Text.ToString());
                SqlParameter enam = new SqlParameter("@Date", TextBox4.Text.ToString());
                SqlParameter duabelas = new SqlParameter("@VesselName", DropDownList3.SelectedItem.ToString());
                SqlParameter empatbelas = new SqlParameter("@GT", TextBox5.Text.ToString());
                SqlParameter tujuh = new SqlParameter("@NextPort", DropDownList4.SelectedItem.ToString());
                SqlParameter delapan = new SqlParameter("@ETD", TextBox6.Text.ToString());
                SqlParameter sembilan = new SqlParameter("@Visc", TextBox7.Text.ToString());
                SqlParameter sepuluh = new SqlParameter("@Density", TextBox8.Text.ToString());
                SqlParameter tigabelas = new SqlParameter("@Flashpoint", TextBox9.Text.ToString());
                SqlParameter limabelas = new SqlParameter("@Sulphur", TextBox10.Text.ToString());
                SqlParameter enambelas = new SqlParameter("@WaterContent", TextBox11.Text.ToString());
                SqlParameter tujuhbelas = new SqlParameter("@GrossVol", TextBox12.Text.ToString());
                SqlParameter delapanbelas = new SqlParameter("@NetVol", TextBox13.Text.ToString());
                SqlParameter sembilanbelas = new SqlParameter("@NetMetric", TextBox14.Text.ToString());
                SqlParameter duapuluh = new SqlParameter("@NetLong", TextBox15.Text.ToString());
                SqlParameter duasatu = new SqlParameter("@NetBarrel", TextBox16.Text.ToString());
                SqlParameter duadua = new SqlParameter("@VCF", TextBox17.Text.ToString());
                SqlParameter duatiga = new SqlParameter("@WCF", TextBox18.Text.ToString());
                SqlParameter duaempat = new SqlParameter("@Temperature", TextBox19.Text.ToString());
                SqlParameter dualima = new SqlParameter("@TblSelling", TextBox20.Text.ToString());
                SqlParameter duaenam = new SqlParameter("@TableMTLT", TextBox21.Text.ToString());
                SqlParameter duatujuh = new SqlParameter("@ForCompanyName", TextBox22.Text.ToString());
                SqlParameter duadelapan = new SqlParameter("@CargoOfficer", TextBox23.Text.ToString());
                SqlParameter duasembilan = new SqlParameter("@Vessel", DropDownList5.SelectedItem.ToString());
                SqlParameter tigapuluh = new SqlParameter("@BunkerTanker", TextBox24.Text.ToString());
                SqlParameter tigasatu = new SqlParameter("@Surveyor", TextBox25.Text.ToString());
                SqlParameter tigadua = new SqlParameter("@MasterChief", TextBox26.Text.ToString());
                SqlParameter tigatiga = new SqlParameter("@Remaks", TextBox27.Text.ToString());

                cmd.Parameters.Add(satu);
                cmd.Parameters.Add(dua);
                cmd.Parameters.Add(tiga);
                cmd.Parameters.Add(empat);
                cmd.Parameters.Add(lima);
                cmd.Parameters.Add(enam);
                cmd.Parameters.Add(tujuh);
                cmd.Parameters.Add(delapan);
                cmd.Parameters.Add(sembilan);
                cmd.Parameters.Add(sepuluh);
                cmd.Parameters.Add(duabelas);
                cmd.Parameters.Add(tigabelas);
                cmd.Parameters.Add(empatbelas);
                cmd.Parameters.Add(limabelas);
                cmd.Parameters.Add(enambelas);
                cmd.Parameters.Add(tujuhbelas);
                cmd.Parameters.Add(delapanbelas);
                cmd.Parameters.Add(sembilanbelas);
                cmd.Parameters.Add(duapuluh);
                cmd.Parameters.Add(duasatu);
                cmd.Parameters.Add(duadua);
                cmd.Parameters.Add(duatiga);
                cmd.Parameters.Add(duaempat);
                cmd.Parameters.Add(dualima);
                cmd.Parameters.Add(duaenam);
                cmd.Parameters.Add(duatujuh);
                cmd.Parameters.Add(duadelapan);
                cmd.Parameters.Add(duasembilan);
                cmd.Parameters.Add(tigapuluh);
                cmd.Parameters.Add(tigasatu);
                cmd.Parameters.Add(tigadua);
                cmd.Parameters.Add(tigatiga);

                con.Open();
                cmd.ExecuteNonQuery();
                // int ReturnCode = (int)cmd.ExecuteScalar();
            }
        }

        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            TextBox2.Text = Calendar1.SelectedDate.ToShortDateString();
        }

        protected void Calendar2_SelectionChanged(object sender, EventArgs e)
        {
            TextBox3.Text = Calendar2.SelectedDate.ToShortDateString();
        }

        protected void Calendar3_SelectionChanged(object sender, EventArgs e)
        {
            TextBox4.Text = Calendar3.SelectedDate.ToShortDateString();
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/FlowMeter.aspx");
        }
    }
}