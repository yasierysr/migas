﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeBehind="BunkerCheckList.aspx.cs" Inherits="MigasLevelUp.BunkerCheckList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .sec {
            margin-left:2em;
            margin-right:2em;
            margin-bottom:1em;
            height:auto 0px;
            padding-top:11.5em;
        }
        .gd {
            margin-bottom:1em;
            width:240px;
            height:40px;
            border:1px ridge lightgrey;
            margin-right:1em;
        }
            .gd:hover {
                border:1px ridge grey;
            }
        .cc {
            margin-right:1em;
        }
        .cc1 {
            margin-left:1em;
            margin-right:1em;
            
        }
        .bb {
             width:240px;
            height:40px;
            border:1px ridge lightgrey;
            padding-left:0.5em;
        }
            .bb:hover {
                 border:1px ridge grey;
                 
            }
        .ff {
            margin-right:1em;
        }
        .ff1 {
            width:240px;
            height:40px;
            border:1px ridge lightgrey;
            padding-left:0.5em;
            margin-bottom:1em;
        }
            .ff1:hover {
                 border:1px ridge grey;
            }
        .sd {
            
            text-align:center;
        }
        .tblchkbunker tr td {
            border:1px ridge grey;
        }
        .tblchkbunker .fg1{
            background-color:lightgrey;
            padding-top:0.5em;
            padding-bottom:0.5em;
            padding-left:1em;
            padding-right:1em;
        }
        .tblchkbunker .fg {
            padding-top:0.5em;
            padding-bottom:0.5em;
            padding-left:1em;
            padding-right:1em;
        }
        .ww {
             width:220px;
            height:40px;
            border:1px ridge lightgrey;
            padding-left:0.5em;
        }
            .ww:hover {
                border:1px ridge grey;
            }
       
        .ttdsender .ty {
            padding-left:0.5em;
            padding-right:0.5em;
            padding-bottom:1em;
            padding-top:1em;
        }
       
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <section class="sec">
        <h4>BUNKER PROCEDURE CHECK LIST</h4><br />
    <div>
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Port of Supply"></asp:Label></td>
                <td>
                    <asp:DropDownList ID="DropDownList1" runat="server" AppendDataBoundItems="true" CssClass="gd">
                        <asp:ListItem Text="Silahkan Pilih" Value="-1"></asp:ListItem>
                    </asp:DropDownList></td>
           <%-- <td>
                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox></td>--%>
                <td>
                    <asp:Label ID="Label42" runat="server" Text="Date" CssClass="cc"></asp:Label></td>
                <td><asp:TextBox ID="TextBox1" runat="server" CssClass="bb"></asp:TextBox></td>
                <td>
                    <asp:Label ID="Label43" runat="server" Text="Time" CssClass="cc1"></asp:Label></td>
                <td><asp:TextBox ID="TextBox2" runat="server" CssClass="bb"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Type of Fuel & Quantity" CssClass="ff"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox4" runat="server" CssClass="ff1"></asp:TextBox></td>
                <td colspan="2">
                    <asp:Label ID="Label44" runat="server" Text="When the Transfered Started"></asp:Label></td>
            </tr>

            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Sender"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox5" runat="server" CssClass="ff1"></asp:TextBox></td>
            </tr>

            <tr>
                <td>
                    <asp:Label ID="Label4" runat="server" Text="Receiver"></asp:Label></td>
                <td>
                    <asp:DropDownList ID="DropDownList2" runat="server" AppendDataBoundItems="true" CssClass="gd">
                        <asp:ListItem Text="Silahkan Pilih" Value="-1"></asp:ListItem>
                    </asp:DropDownList></td>
            <%--<td>
                <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox></td>--%>
            </tr>
        </table>
        
    </div>
    <div>
        <table class="tblchkbunker">
            <tr>
                <td class="fg1"></td>
                <td class="fg1">
                    <asp:Label ID="Label5" runat="server" Text="Pre-Bunkering Procedure/Prosedur Sebelum Pengisian Bahan Bakar"></asp:Label></td>
                <td class="fg1">
                    <asp:Label ID="Label6" runat="server" Text="Sender" CssClass="sd"></asp:Label></td>
                <td class="fg1">
                    <asp:Label ID="Label7" runat="server" Text="Receiver" CssClass="sd"></asp:Label></td>
            </tr>
            <tr>
                <td class="fg">
                    <asp:Label ID="Label45" runat="server" Text="1" CssClass="number"></asp:Label></td>
                <td class="fg">
                    <asp:Label ID="Label8" runat="server" Text="Agree quantity to be supplied / Setuju kuantitas yang akan dipasok"></asp:Label></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox1" runat="server" CssClass="sd1" /></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox2" runat="server" CssClass="sd1" /></td>
            </tr>
            <tr>
                <td class="fg">
                    <asp:Label ID="Label46" runat="server" Text="2" CssClass="number"></asp:Label></td>
                <td class="fg">
                    <asp:Label ID="Label9" runat="server" Text="Mooring/station keeping ability satisfactory /  Kepil / stasiun menjaga keseimbangan sdh  memuaskan"></asp:Label></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox3" runat="server" CssClass="sd1" /></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox4" runat="server" CssClass="sd1" /></td>
            </tr>
            <tr>
                <td class="fg">
                    <asp:Label ID="Label47" runat="server" Text="3" CssClass="number"></asp:Label></td>
                <td class="fg">
                    <asp:Label ID="Label10" runat="server" Text="Oil transfer hose sufficient length / Selang minyak untuk transfer yang cukup panjang"></asp:Label></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox5" runat="server" CssClass="sd1" /></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox6" runat="server" CssClass="sd1" /></td>
            </tr>
            <tr>
                <td class="fg">
                    <asp:Label ID="Label48" runat="server" Text="4" CssClass="number"></asp:Label></td>
                <td class="fg">
                    <asp:Label ID="Label11" runat="server" Text="Oil transfer hose properly supported / Selang mendukung untuk transfer minyak"></asp:Label></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox7" runat="server" CssClass="sd1" /></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox8" runat="server" CssClass="sd1" /></td>
            </tr>
            <tr>
                <td class="fg">
                    <asp:Label ID="Label49" runat="server" Text="5" CssClass="number"></asp:Label></td>
                <td class="fg">
                    <asp:Label ID="Label12" runat="server" Text="Transfer hose connected to vessel’s fixed transfer system / Selang Transfer  terhubung ke sistem transfer tetap kapal"></asp:Label></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox9" runat="server" CssClass="sd1" /></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox10" runat="server" CssClass="sd1" /></td>
            </tr>
            <tr>
                <td class="fg">
                    <asp:Label ID="Label50" runat="server" Text="6" CssClass="number"></asp:Label></td>
                <td class="fg">
                    <asp:Label ID="Label13" runat="server" Text="Oil transfer hose inspected for satisfactory condition / selang transfer Minyak diperiksa dlm kondisi memuaskan"></asp:Label></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox11" runat="server" CssClass="sd1" /></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox12" runat="server" CssClass="sd1" /></td>
            </tr>
            <tr>
                <td class="fg">
                    <asp:Label ID="Label51" runat="server" Text="7" CssClass="number"></asp:Label></td>
                <td class="fg">
                    <asp:Label ID="Label14" runat="server" Text="Agree start/stop signals between vessel and barge/ Setuju sinyal  start / stop antara kapal dan tongkang"></asp:Label></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox13" runat="server" CssClass="sd1" /></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox14" runat="server" CssClass="sd1" /></td>
            </tr>
            <tr>
                <td class="fg">
                    <asp:Label ID="Label52" runat="server" Text="8" CssClass="number"></asp:Label></td>
                <td class="fg">
                    <asp:Label ID="Label15" runat="server" Text="Foam Fire extinguisher placed at position / Pemadam Api sudah dlm posisi"></asp:Label></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox15" runat="server" CssClass="sd1" /></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox16" runat="server" CssClass="sd1" /></td>
            </tr>
            <tr>
                <td class="fg">
                    <asp:Label ID="Label53" runat="server" Text="9" CssClass="number"></asp:Label></td>
                <td class="fg">
                    <asp:Label ID="Label16" runat="server" Text="Check barge meters / Cek Flow meter Tonkang | Reading :"></asp:Label></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox17" runat="server" CssClass="sd1" /></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox18" runat="server" CssClass="sd1" /></td>
            </tr>
            <tr>
                <td class="fg">
                    <asp:Label ID="Label54" runat="server" Text="10" CssClass="number"></asp:Label></td>
                <td class="fg">
                    <asp:Label ID="Label17" runat="server" Text="Check on board meters/Cek Flow meter Kapal | Reading :"></asp:Label></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox19" runat="server" CssClass="sd1" /></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox20" runat="server" CssClass="sd1" /></td>
            </tr>
            <tr>
                <td class="fg">
                    <asp:Label ID="Label55" runat="server" Text="11" CssClass="number"></asp:Label></td>
                <td class="fg">
                    <asp:Label ID="Label18" runat="server" Text="Witness taking and sealing of product samples / Pengambilan sampel produk"></asp:Label></td>
            <td class="fg">
                <asp:CheckBox ID="CheckBox21" runat="server" CssClass="sd1" /></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox22" runat="server" CssClass="sd1" /></td>
            </tr>
            <tr>
                <td class="fg">
                    <asp:Label ID="Label56" runat="server" Text="12" CssClass="number"></asp:Label></td>
                <td class="fg">
                    <asp:Label ID="Label19" runat="server" Text="Communication available / alat komunikasi tersedia"></asp:Label></td>
            <td class="fg">
                <asp:CheckBox ID="CheckBox23" runat="server" CssClass="sd1" /></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox24" runat="server" CssClass="sd1" /></td>
            </tr>
            <tr>
                <td class="fg">
                    <asp:Label ID="Label57" runat="server" Text="13" CssClass="number"></asp:Label></td>
                <td class="fg">
                    <asp:Label ID="Label20" runat="server" Text="Required personnel on duty / Ada petugas siap jaga"></asp:Label>
                </td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox25" runat="server" CssClass="sd1" /></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox26" runat="server" CssClass="sd1" /></td>
            </tr>
            <tr>
                <td class="fg">
                    <asp:Label ID="Label58" runat="server" Text="14" CssClass="number"></asp:Label></td>
                <td class="fg">
                    <asp:Label ID="Label21" runat="server" Text="Both persons in charge agree to begin transfer and the transfers rate / Kedua orang yang bertanggung jawab setuju untuk memulai transfer dan rate / tingkat transfer"></asp:Label></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox27" runat="server" CssClass="sd1" /></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox28" runat="server" CssClass="sd1" /></td>
            </tr>
            <tr>
                <td class="fg">
                    <asp:Label ID="Label59" runat="server" Text="15" CssClass="number"></asp:Label></td>
                <td class="fg">
                    <asp:Label ID="Label22" runat="server" Text="Warning signs posted (No Smoking ) / Ada tanda peringatan (Dilarang Merokok)"></asp:Label></td>
            <td class="fg">
                <asp:CheckBox ID="CheckBox29" runat="server" CssClass="sd1" /></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox30" runat="server" CssClass="sd1" /></td>
            </tr>
            <tr>
                <td class="fg">
                    <asp:Label ID="Label60" runat="server" Text="16" CssClass="number"></asp:Label></td>
                <td class="fg">
                    <asp:Label ID="Label23" runat="server" Text="During Bunkering Procedure / Prosedur Selama pengisisan bahan bakar"></asp:Label></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox31" runat="server" CssClass="sd1" /></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox32" runat="server" CssClass="sd1" /></td>
            </tr>
            <tr>
                <td class="fg">
                    <asp:Label ID="Label61" runat="server" Text="17" CssClass="number"></asp:Label></td>
                <td class="fg">
                    <asp:Label ID="Label24" runat="server" Text="Monitor Fuel connections for leaks fuel flows / Dimonitor apabila ada kebocoran di selang waktu pengaliran"></asp:Label>
                </td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox33" runat="server" CssClass="sd1" /></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox34" runat="server" CssClass="sd1" /></td>
            </tr>
            <tr>
                <td class="fg">
                    <asp:Label ID="Label62" runat="server" Text="18" CssClass="number"></asp:Label></td>
                <td class="fg"><asp:Label ID="Label25" runat="server" Text="Procedure on Completion of Bunkering / Prosedur Saat Selesai Pengisian BBM"></asp:Label></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox35" runat="server" CssClass="sd1" /></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox36" runat="server" CssClass="sd1" /></td>
            </tr>
            <tr>
                <td class="fg"> <asp:Label ID="Label63" runat="server" Text="19" CssClass="number"></asp:Label></td>              
                <td class="fg">
                    <asp:Label ID="Label26" runat="server" Text="Bunker Valve closed / Kerangan bunker ditutup"></asp:Label></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox37" runat="server" CssClass="sd1" /></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox38" runat="server" CssClass="sd1" /></td>
            </tr>
            <tr>
                <td class="fg">
                    <asp:Label ID="Label64" runat="server" Text="20" CssClass="number"></asp:Label></td>
                <td class="fg">
                    <asp:Label ID="Label27" runat="server" Text="Check barge meters / Cek Flow meter Tonkang Reading :"></asp:Label></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox39" runat="server" CssClass="sd1" /></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox40" runat="server" CssClass="sd1" /></td>
            </tr>
            <tr>
                <td class="fg">
                    <asp:Label ID="Label65" runat="server" Text="21" CssClass="number"></asp:Label></td>
                <td class="fg">
                    <asp:Label ID="Label28" runat="server" Text="Check on board meters / Cek Flowmeter Kapal Reading :"></asp:Label></td>
            <td class="fg">
                <asp:CheckBox ID="CheckBox41" runat="server" CssClass="sd1" /></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox42" runat="server" CssClass="sd1" /></td>
            </tr>
            <tr>
                <td class="fg">
                    <asp:Label ID="Label66" runat="server" Text="22" CssClass="number"></asp:Label></td>
                <td class="fg">
                    <asp:Label ID="Label29" runat="server" Text="Sign Bunker Delivery Receipt / Tanda Tangan Dokumen Bunker | BDR 128 /JE-BDR/CUST/ VII /2010"></asp:Label></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox43" runat="server" CssClass="sd1" /></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox44" runat="server" CssClass="sd1" /></td>
            </tr>
            <tr>
                <td class="fg">
                    <asp:Label ID="Label67" runat="server" Text="23" CssClass="number"></asp:Label></td>
                <td class="fg">
                    <asp:Label ID="Label30" runat="server" Text="Master informed of completion / Pemberitahuan Selesai Pengisian BBM oleh Master"></asp:Label></td>
            <td class="fg">
                <asp:CheckBox ID="CheckBox45" runat="server" CssClass="sd1" /></td>
                <td class="fg">
                    <asp:CheckBox ID="CheckBox46" runat="server" CssClass="sd1" /></td>
            </tr>
        </table>
    </div>
        <br />
    <div>
        <table class="ttdsender">
            <tr>
                <td class="ty">
                    <asp:Label ID="Label31" runat="server" Text="SENDER/Pengirim"></asp:Label></td>
                <td class="ty">
                    <asp:Label ID="Label32" runat="server" Text="RECEIVER/Penerima"></asp:Label></td>
                <td class="ty">
                    <asp:Label ID="Label33" runat="server" Text="Date"></asp:Label></td>
                <td class="ty">
                    <asp:TextBox ID="TextBox7" runat="server" CssClass="ww"></asp:TextBox></td>
                <td class="ty">
                    <asp:Label ID="Label34" runat="server" Text="Time"></asp:Label></td>
                <td class="ty">
                    <asp:TextBox ID="TextBox8" runat="server" CssClass="ww"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="ty"></td>
                <td class="ty"></td>
                <td class="ty" colspan="2">
                    <asp:Label ID="Label35" runat="server" Text="(when the transfer completed)"></asp:Label></td>
                
            </tr>
            <tr>
                <td class="ty">
                    <asp:TextBox ID="TextBox9" runat="server" CssClass="ww"></asp:TextBox></td>
                <td class="ty">
                    <asp:TextBox ID="TextBox10" runat="server" CssClass="ww"></asp:TextBox></td>
                <td class="ty">
                    <asp:TextBox ID="TextBox11" runat="server" CssClass="ww"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="ty">
                    <asp:Label ID="Label36" runat="server" Text="Name and Signature"></asp:Label></td>
                 <td class="ty">
                    <asp:Label ID="Label37" runat="server" Text="Name and Signature"></asp:Label></td>
                 <td class="ty">
                    <asp:Label ID="Label38" runat="server" Text="Name and Signature"></asp:Label></td>
            </tr>
            <tr>
                <td class="ty">
                    <asp:Label ID="Label39" runat="server" Text="Title"></asp:Label></td>
            <td class="ty">
                    <asp:Label ID="Label40" runat="server" Text="Title"></asp:Label></td>
                <td class="ty">
                    <asp:Label ID="Label41" runat="server" Text="Title"></asp:Label></td>
            </tr>
        </table>
    </div>
        </section>
</asp:Content>
