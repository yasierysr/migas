﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeBehind="MasterBuyerListing.aspx.cs" Inherits="MigasLevelUp.MasterBuyerListing" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .pikaSearch {
           height:40px;
           padding-left:0.2em; 
           border:1px solid dimgray;
           margin-right:0.2em;
           margin-left:72em;
           margin-bottom:0.2em;
        }
            .pikaSearch:hover {
                border:1px solid darkslateblue; 
            }
        .pikaBtnSearch {
            border:none;
            font-family:Opensan;
            font-size: large;
            border-radius:5px;
            background-color: ActiveBorder;
            width:120px;
            height:40px;
            
        }
            .pikaBtnSearch:hover {
                background-color:ActiveCaption;
            }
            @font-face {
   font-family: Opensan;
   src: url('../assets/fonts/OpenSans-Regular.ttf');
}
            .MSTR {
            padding-top:10.5em;
        }
             .edititem {
             width:120px;
             height:40px;
             padding-left:0.5em;
             border:1px ridge lightgrey;
         }
             .edititem:hover {
                  border:1px ridge grey;
             }
             .page_enabled, .page_disabled{
            display: inline-block;
            height: 25px;
            min-width: 25px;
            line-height: 25px;
            text-align: center;
            text-decoration: none;
            border: 1px solid;
            border-style:none;
            width: 40px;
            
        }
        .page_enabled {
            background-color: firebrick;
            color: lightblue;
            font-family: 'Agency FB';
        }
        .page_disabled {
            background-color: chocolate;
            color: black;
        }

        .ChildGrid td
        {
            background-color: lightblue;
            color: black;
            font-size: 10pt;
            line-height:200%;
            text-align:center;
        }
        .ChildGrid th
        {
            background-color: sandybrown;
            color: White;
            font-size: 10pt;
            line-height:200%;
            text-align:center;
        }
        .yasier {
        }

    </style>
  
    <script type="text/javascript" src="assets/js/Jquery1.8.3.js"></script>
    <script type="text/javascript">
        $("[src*=plus]").live("click", function () {
            $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>")
            $(this).attr("src", "assets/img/minus.png");
        });
        $("[src*=minus]").live("click", function () {
            $(this).attr("src", "assets/img/plus.png");
            $(this).closest("tr").next().remove();
        });
    </script>
     <script src="assets/js/jquery-1.10.2.js"></script>
    <script src="assets/js/Jquery.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var gridHeader = $('#<%=GridView1.ClientID%>').clone(true);
            $(gridHeader).find("tr:gt(0)").remove();
            $('#<%=GridView1.ClientID%> tr th').each(function (i) {
                $("th:nth-child(" + (i + 1) + ")", gridHeader).css('width', $(this).width().toString() + "px");
            });
            $("#GHead").append(gridHeader);
            $('#GHead').css('position', 'absolute');
            $('#GHead').css('top', $('#<%=GridView1.ClientID%>').offset().top);
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="MSTR">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#TabelSellingList"
                    style="line-height: 35px;" class="text-bold text-decoration-none">Tabel Selling List
                </a>
            </h4>
        </div>
        <div id="TabelSellingList" class="panel-body panel-collapse in">
            <div class="table-responsive">
                <asp:TextBox ID="TextBoxSearch" runat="server" CssClass="pikaSearch"></asp:TextBox><asp:Button ID="Button1" runat="server" Text="Search" OnClick="Button1_Click" CssClass="pikaBtnSearch" /> 
                <div id="GHead"></div>
                    <div style="height:350px; overflow:auto">               
        <asp:GridView class="table table-striped table-bordered" ID="GridView1" runat="server" AutoGenerateColumns="false" CellPadding="11" CellSpacing="1" GridLines="None" ShowHeader="true"
                    HeaderStyle-CssClass="FixedHeader" AlternatingRowStyle-BackColor="LightBlue" RowStyle-BackColor="LightBlue" DataKeyNames="Id"
                    OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating"
                    OnRowDataBound="GridView1_RowDataBound" OnSorting="GridView1_Sorting" AllowSorting="true" OnDataBound="GridView1_DataBound">
            <Columns>
                <asp:TemplateField ControlStyle-CssClass="yasier">
                <ItemTemplate>
                    <img alt = "" style="cursor: pointer" src="assets/img/plus.png" width="20" class="rieder" />
                    <asp:Panel ID="pnlOrders" runat="server" Style="display: none">
                        <asp:GridView ID="gvOrders" runat="server" AutoGenerateColumns="false" CssClass = "ChildGrid">
                            <Columns>
                                <asp:BoundField ItemStyle-Width="150px" DataField="CommencedPumping" HeaderText="Commenced Pumping" />
                                <asp:BoundField ItemStyle-Width="150px" DataField="CompletedPumping" HeaderText="Completed Pumping" />
                                <asp:BoundField ItemStyle-Width="150px" DataField="GT" HeaderText="GT" />
                                <asp:BoundField ItemStyle-Width="150px" DataField="ETD" HeaderText="ETD" />
                                <asp:BoundField ItemStyle-Width="150px" DataField="Visc" HeaderText="Visc" />
                                <asp:BoundField ItemStyle-Width="150px" DataField="Density" HeaderText="Density" />
                                <asp:BoundField ItemStyle-Width="150px" DataField="Flashpoint" HeaderText="ETD" />
                                <asp:BoundField ItemStyle-Width="150px" DataField="Sulphur" HeaderText="Sulphur" />
                                <asp:BoundField ItemStyle-Width="150px" DataField="WaterContent" HeaderText="WaterContent" />
                                <asp:BoundField ItemStyle-Width="150px" DataField="GrossVol" HeaderText="GrossVol" />
                                <asp:BoundField ItemStyle-Width="150px" DataField="NetVol" HeaderText="NetVol" />
                                <asp:BoundField ItemStyle-Width="150px" DataField="NetMetric" HeaderText="NetMetric" />
                                
                            </Columns>
                        </asp:GridView>
                        <asp:GridView ID="gvOrders1" runat="server" AutoGenerateColumns="false" CssClass = "ChildGrid">
                            <Columns>
                                <asp:BoundField ItemStyle-Width="150px" DataField="NetLong" HeaderText="NetLong" />
                                <asp:BoundField ItemStyle-Width="150px" DataField="VCF" HeaderText="VCF" />
                                <asp:BoundField ItemStyle-Width="150px" DataField="WCF" HeaderText="WCF" />
                                <asp:BoundField ItemStyle-Width="150px" DataField="Temperature" HeaderText="Temperature" />
                                <asp:BoundField ItemStyle-Width="150px" DataField="TblSelling" HeaderText="TblSelling" />
                                <asp:BoundField ItemStyle-Width="150px" DataField="TableMTLT" HeaderText="Table MTLT" />
                                <asp:BoundField ItemStyle-Width="150px" DataField="ForCompanyName" HeaderText="For Company Name" />
                                <asp:BoundField ItemStyle-Width="150px" DataField="CargoOfficer" HeaderText="Cargo Officer" />
                                <asp:BoundField ItemStyle-Width="150px" DataField="Vessel" HeaderText="Vessel" />
                                <asp:BoundField ItemStyle-Width="150px" DataField="CargoOfficer" HeaderText="Cargo Officer" />
                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
                </ItemTemplate>
            </asp:TemplateField>
            </Columns>
                    <Columns>
                        <asp:TemplateField HeaderText="ID" HeaderStyle-CssClass="penyu" ItemStyle-CssClass="bintanglaut" SortExpression="Id">
                            <ItemTemplate>
                                <%#Eval("Id") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <%--<Columns>
                        <asp:TemplateField HeaderText="ID" HeaderStyle-CssClass="penyu" ItemStyle-CssClass="bintanglaut" SortExpression="Id">
                            <ItemTemplate>
                                <%#Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>--%>
                    <Columns>
                        <asp:TemplateField HeaderText="Delivered At" HeaderStyle-CssClass="penyu" ItemStyle-CssClass="bintanglaut" SortExpression="DeliveredAt">
                            <ItemTemplate>
                                <%#Eval("DeliveredAt") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TxtDeliveredAt" runat="server" CssClass="edititem" Text='<%#Bind("DeliveredAt") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:TemplateField HeaderText="Delivered By" HeaderStyle-CssClass="penyu" ItemStyle-CssClass="bintanglaut" SortExpression="DeliveredBy">
                            <ItemTemplate>
                                <%#Eval("DeliveredBy") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TxtDeliveredBy" runat="server" CssClass="edititem" Text='<%#Bind("DeliveredBy") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:TemplateField HeaderText="Grade" HeaderStyle-CssClass="penyu1" ItemStyle-CssClass="bintanglaut" SortExpression="Grade">
                            <ItemTemplate>
                                <%#Eval("Grade") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TxtGrade" runat="server" CssClass="edititem" Text='<%#Bind("Grade") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:TemplateField HeaderText="Vessel Name" HeaderStyle-CssClass="penyu" ItemStyle-CssClass="bintanglaut" SortExpression="VesselName">
                            <ItemTemplate>
                                <%#Eval("VesselName") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TxtVesselName" runat="server" CssClass="edititem" Text='<%#Bind("VesselName") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:TemplateField HeaderText="Next Port" HeaderStyle-CssClass="penyu" ItemStyle-CssClass="bintanglaut" SortExpression="NextPort">
                            <ItemTemplate>
                                <%#Eval("NextPort") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TxtNextPort" runat="server" CssClass="edititem" Text='<%#Bind("NextPort") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:TemplateField HeaderText="Date" HeaderStyle-CssClass="penyu" ItemStyle-CssClass="bintanglaut" SortExpression="Date">
                            <ItemTemplate>
                                <%#Eval("Date") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TxtDate" runat="server" CssClass="edititem" Text='<%#Bind("Date") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <Columns>
                        <asp:CommandField ButtonType="Image" ShowEditButton="true" ShowCancelButton="true" EditImageUrl="~/assets/img/edit.png" CancelImageUrl="~/assets/img/cancel.png" UpdateImageUrl="~/assets/img/update.png"  ControlStyle-Height="30px" ControlStyle-Width="30px" ItemStyle-Width="50px" ItemStyle-Height="50px" ItemStyle-CssClass="dugtrio" ControlStyle-CssClass="golduck" />
                    </Columns>
                    <Columns>
                        <asp:ButtonField ButtonType="Image" Text="Print" ControlStyle-Height="30px" ControlStyle-Width="30px" ImageUrl="~/assets/img/print1.png" ItemStyle-Width="50px" ItemStyle-Height="50px" ItemStyle-CssClass="dugtrio" ControlStyle-CssClass="golduck" />
                    </Columns>
                    <Columns>
                        <asp:CommandField ButtonType="Image" ShowDeleteButton="true" DeleteImageUrl="~/assets/img/delete1.png" ControlStyle-Height="30px" ControlStyle-Width="30px" ItemStyle-Width="50px" ItemStyle-Height="50px" ItemStyle-CssClass="dugtrio" ControlStyle-CssClass="golduck" />
                    </Columns>
                </asp:GridView>
                        </div>
                </div>
        </div>
        </div>
        </section>
</asp:Content>
