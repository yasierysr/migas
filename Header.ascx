﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Header.ascx.cs" Inherits="WebUserControl1" %>

<div class="navbar navbar-inverse set-radius-zero" id="pikachu">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="Default.aspx">

                <img src="assets/img/logo_jagad2.jpg" width="60" height="60" /><b class="JE">PT Jagad Energy</b>
            </a>
            
        </div>

        <div class="right-div">
            <asp:Button ID="btnlogout" class="btn btn-danger pull-right" runat="server" Text="LOG OUT" OnClick="btnlogout_Click" />
    
        </div>
    </div>
</div>
<!-- LOGO HEADER END-->
<section class="menu-section" id="raichu">
    <div class="container">
        <div class="row ">
            <div class="col-md-12">
                <div class="navbar-collapse collapse ">
                    <ul id="menu-top" class="nav navbar-nav">
                        <li><a href="Default.aspx" class="dropdown-toggle" id="menu-top-active" data-toggle="dropdown">MASTER<i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="ddlmenuItem">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="MasterSellerListing.aspx">SELLER LISTING</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="MasterBuyerListing.aspx">BUYER LISTING</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="VesselName.aspx">VESSEL'S NAME</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="ui.html">BUNKER TANKER TYPE</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="Grade.aspx">GRADE</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="PortName.aspx">PORT NAME</a></li>
                            </ul>
                        </li>

                        <li>
                            <a href="form.html" class="dropdown-toggle" id="ddlmenuItempPlanning" data-toggle="dropdown">PLANNING<i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="ddlmenuItem">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="NewPlanning.aspx">NEW</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">VIEW</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="ui.html">REPORT</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="dropdown-toggle" id="ddlmenuItem" data-toggle="dropdown">BUYING <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="ddlmenuItem">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="NewBuying.aspx">NEW</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">VIEW</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="ReportBuying.aspx">REPORT</a></li>
                            </ul>
                        </li>
                        <li><a href="tab.html" class="dropdown-toggle" id="ddlmenuItemSelling" data-toggle="dropdown">SELLING<i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="ddlmenuItem">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="NewSelling.aspx">NEW</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">VIEW</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="ReportSelling.aspx">REPORT</a></li>
                            </ul>
                        </li>
                        <li><a href="table.html" class="dropdown-toggle" id="ddlmenuItemReport" data-toggle="dropdown">REPORT<i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="ddlmenuItem">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">PLANNING</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="ReportBuying.aspx">BUYING</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="ReportSelling.aspx">SELLING</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="ui.html">OPERATIONAL</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">AUDIT TRAIL</a></li>
                            </ul>
                        </li>
                        <li><a href="blank.html" class="dropdown-toggle" id="ddlmenuItemUpload" data-toggle="dropdown">UPLOAD<i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="ddlmenuItem">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="ui.html">UPLOAD REPORT</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">ACTIVITY REPORT</a></li>
                            </ul>
                        </li>
                        <li><a href="blank.html" class="dropdown-toggle" id="A1" data-toggle="dropdown">SETTING<i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="ddlmenuItem">
                                <li role="presentation" class="se1"><a role="menuitem" tabindex="-1" href="ui.html">SYSTEM PARAMETER</a></li>
                                <li role="presentation" class="se1"><a role="menuitem" tabindex="-1" href="#">GROUP</a>
                                    <ul class="z1">
                                        <li role="presentation" class="sa1"><a role="menuitem" tabindex="-1" href="#" class="create">CREATE</a></li>
                                        <li role="presentation" class="sa2"><a role="menuitem" tabindex="-1" href="#" class="view">VIEW</a></li>
                                    </ul>
                                </li>
                                <li role="presentation" class="se1"><a role="menuitem" tabindex="-1" href="#">USER</a>
                                     <ul class="z1">
                                        <li role="presentation" class="sa1"><a role="menuitem" tabindex="-1" href="#" class="create">CREATE</a></li>
                                        <li role="presentation" class="sa2"><a role="menuitem" tabindex="-1" href="#" class="view">VIEW</a></li>
                                    </ul>
                                </li>
                                <li role="presentation" class="se1"><a role="menuitem" tabindex="-1" href="#">CHANGE PASSWORD</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- MENU SECTION END-->
