﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeBehind="VesselName.aspx.cs" Inherits="MigasLevelUp.VesselName" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .btnvesselName {
            width:120px;
            height:40px;
            margin-top:0.5em;
            margin-bottom:1em;
            color:white;
            background-color:lightskyblue;
            border:none;
        }
            .btnvesselName:hover {
                background-color: deepskyblue;
            }
        .vesseldiv {
            /*margin-top:1em;*/
            padding-top:11.5em;
            margin-bottom:1em;
            margin-left:2em;
        }
        .txtvessel {
            width:180px;
            height:40px;
            padding-left:0.5em;
            border:1px ridge lightgrey;
        }
            .txtvessel:hover {
                border:1px ridge cornflowerblue;
            }
        .lblvessel {
            margin-right:1em;
        }        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="vesseldiv">
        <table class="vesseltable">
            <tr class="vesseltr">
                <td><asp:Label ID="Label1" runat="server" Text="Vessel Name" CssClass="lblvessel"></asp:Label></td>
                <td><asp:TextBox ID="TextBox1" runat="server" CssClass="txtvessel"></asp:TextBox></td>
            </tr>
            <tr>
                <td></td>
                <td><asp:Button ID="Button1" runat="server" Text="Save" CssClass="btnvesselName" OnClick="Button1_Click" /></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="Label2" runat="server"></asp:Label></td>
            </tr>
        </table>
    
        </div>
</asp:Content>
