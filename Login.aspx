﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Migas.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Login</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <%--<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css" />
    <link href="assets/css/style.css" rel="stylesheet" />
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    --%>
    <script src="js/Jquery.js"></script>
    <style>
.body-login {
    background-image:url('../assets/img/LevelUp2.jpg');
    background-size:cover;
    background-repeat:no-repeat;
    height:auto 0px;
    margin: 0;
    padding:0;
}
@media 
only screen and (max-width: 760px),
(min-device-width: 768px) and (max-device-width: 1024px)  {
            .body-login {
                background-image: url('../assets/img/LevelUp2.jpg');
                height: auto 0px;
                background-size:contain;
                
            }
            .LoginBox {
            position:absolute;
           margin-left:5.1em; 
           margin-top:-5em;
            width:350px;
            box-sizing:border-box;
            background-color: rgba(0,0,0,0.5);
            border-radius:5px;
            transition: .5s;
            box-shadow: 0 10px 20px rgba(0,0,0,.5);
        }
           
        }
        .LoginBox {
            position:absolute;
            top:55%;
            left:20%;
            transform: translate(-50%,-50%);
            width:350px;
            box-sizing:border-box;
            background-color: rgba(0,0,0,0.5);
            border-radius:5px;
            transition: .5s;
            box-shadow: 0 10px 20px rgba(0,0,0,.5);
        }
            .LoginBox:hover {
                box-shadow: 0 10px 20px rgba(0,0,0,0);
                background: rgba(255,255,255,1);
            }
        .Glass {
            width:100%;
            height:100%;
            padding: 40px;
            box-sizing:border-box;
            background: rgba(255,255,255,.1);
            border-radius:5px;
            transition:.5s;
        }
        .User {
            margin:0 auto;
            display:block;
            margin-bottom:20px;
        }
        h4 {
            margin:0;
            padding: 0 0 20px;
            color: white;
            text-align:center;
            font-family:afont;
            text-shadow: 0 0 3px #FF0000, 0 0 5px #0000FF;
        }
        .LoginBox .chihua {
            width:100%;
            margin-bottom:20px;
            border:none;
            outline:none;
            height:40px;
            color:#262626;
            font-size:12px;
            padding-left:20px;
            box-sizing:border-box;
            /*font-family:afont;*/
            border:1px solid blueviolet;
            border-radius:5px;
        }
        .chihua:hover {
            border:1px solid dodgerblue;
            
        }
        .InputBox {
            position:relative;
        }
            .InputBox span {
                position:absolute;
                top: 10px;
                left:5px;
                color:#262626;
            }
        .pokemon {
            width:100%;
            outline:none;
            border:none;
            border-radius:20px;
            cursor:pointer;
            height:40px;
            margin-bottom:20px;
            background-color:dodgerblue;
            color:white;
            font-family:afont;
        }
            .pokemon:hover {
                background-color:deepskyblue;
            }
        .forgotpwd {
            margin-left:3em;
            text-decoration:none;
            cursor:pointer;
            font-family:afont;
            font-size: small;
        }
        .forgotpwd:hover {
            text-decoration:underline;
        }
         @font-face {
                font-family: afont;
                src: url('../assets/fonts/ethnocentric1.ttf');
            }

            @font-face {
                font-family: Opensan;
                src: url('../assets/fonts/OpenSans-Regular.ttf');
            }
        
    </style>
</head>
<body class="body-login">
    <form id="form1" runat="server">
               
        <div class="LoginBox">
            <div class="Glass">
                <img src="assets/img/unlock-icon.png" width="50" height="50" class="User" />
                <h4>Sign in here</h4>
                <form>
                    <div class="InputBox">
                        <span><i class="fa fa-user" aria-hidden="true"></i></span>
                        <asp:TextBox ID="TextBox1" runat="server" CssClass="chihua" placeholder="Username"></asp:TextBox>
                        
                    </div>
                     <div class="InputBox">
                         <span><i class="fa fa-lock" aria-hidden="true"></i></span>
                        <asp:TextBox ID="TextBox2" runat="server" CssClass="chihua" placeholder="Password" TextMode="Password"></asp:TextBox>
                        
                    </div>
                    <asp:Button ID="Button1" runat="server" Text="Sign In" CssClass="pokemon" OnClick="Button1_Click" />
                </form><br />
                <a href="#" class="forgotpwd">Forgot Password</a>
            </div>
        </div>
            
    </form>
</body>
</html>
