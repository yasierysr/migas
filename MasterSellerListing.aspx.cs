﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;

namespace MigasLevelUp
{
    public partial class MasterSellerListing : System.Web.UI.Page
    {
     
        string sql = ConfigurationManager.ConnectionStrings["OilGas"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillGridViewSelling();
                gvbind();
                BindMasterSellingDetails();
                GridView1.DataSource = GetData("select top 10 * from Selling");
             //   GridView1.DataSource = GetData("select * from Buying");
                GridView1.DataBind();
            }
        }
        private static DataTable GetData(string query)
        {
            string strConnString = ConfigurationManager.ConnectionStrings["OilGas"].ConnectionString;
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = query;
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        using (DataSet ds = new DataSet())
                        {
                            DataTable dt = new DataTable();
                            sda.Fill(dt);
                            return dt;
                        }
                    }
                }
            }
        }
        private void FillGridViewSelling()
        {

            using (SqlConnection con = new SqlConnection(sql))
            {
                SqlCommand cmd = new SqlCommand("Select * from Selling", con);
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                GridView1.DataSource = rdr;
                GridView1.DataBind();
            }
        }

        protected void gvbind()
        {
            using (SqlConnection con = new SqlConnection(sql))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from Selling", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                con.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    GridView1.DataSource = ds;
                    GridView1.DataBind();

                }
                else
                {
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    GridView1.DataSource = ds;
                    GridView1.DataBind();
                    int columncount = GridView1.Rows[0].Cells.Count;
                    GridView1.Rows[0].Cells.Clear();
                    GridView1.Rows[0].Cells.Add(new TableCell());
                    GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                    GridView1.Rows[0].Cells[0].Text = "No Records Found";
                }
            }
        }
        #region "Bind Selling Details"
        private DataTable BindMasterSellingDetails()
        {
            using (SqlConnection con = new SqlConnection(sql))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("select * from Selling", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                con.Close();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GridView1.DataSource = ds;
                    GridView1.DataBind();
                }
                else
                {

                }
                return ds.Tables[0];
            }
        }
        #endregion
        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            FillGridViewSelling();
          //  gvbind();
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = (GridViewRow)GridView1.Rows[e.RowIndex];
            using (SqlConnection con = new SqlConnection(sql))
            {

                if (GridView1.Rows.Count > 0)
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("delete FROM Selling where Id='" + Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value.ToString()) + "'", con);
                    cmd.ExecuteNonQuery();
                    con.Close();
                    gvbind();

                }
                else
                {
                    gvbind();
                    //GridView1.Attributes.Add("OnClick", "return confirm('Really wanna delete?');");
                }
            }
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            using (SqlConnection con = new SqlConnection(sql))
            {
                string query = "select [DeliveredAt],[DeliveredBy],[Grade],[VesselName],[NextPort],[Date] from Selling where [Id]=N'" + GridView1.Rows[e.NewEditIndex].Cells[1].Text + "'";
                con.Open();
                SqlDataAdapter adp = new SqlDataAdapter(query, con);
                DataSet ds = new DataSet();
                adp.Fill(ds);
                if (con != null)
                {
                    con.Close();
                }
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            gvbind();

        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int userid = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value.ToString());
            GridViewRow row = (GridViewRow)GridView1.Rows[e.RowIndex];

            TextBox DeliveredAt = (TextBox)row.FindControl("TxtDeliveredAt");
            TextBox DeliveredBy = (TextBox)row.FindControl("TxtDeliveredBy");
            TextBox Grade = (TextBox)row.FindControl("TxtGrade");
            TextBox VesselName = (TextBox)row.FindControl("TxtVesselName");
            TextBox NextPort = (TextBox)row.FindControl("TxtNextPort");
            TextBox Date = (TextBox)row.FindControl("TxtDate");

            GridView1.EditIndex = -1;

            using (SqlConnection con = new SqlConnection(sql))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("update Selling set DeliveredAt='" + DeliveredAt.Text + "',DeliveredBy='" + DeliveredBy.Text + "',Grade='" + Grade.Text + "',VesselName='" + VesselName.Text + "',NextPort='" + NextPort.Text + "',Date='" + Date.Text + "' where Id='" + userid + "'", con);
                cmd.ExecuteNonQuery();
                con.Close();
                gvbind();
            }

        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirstate"] == null)
                {
                    ViewState["dirstate"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirstate"];
            }
            set
            {
                ViewState["dirstate"] = value;
            }
        }
        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            string sortingDirection = string.Empty;
            if (direction == SortDirection.Ascending)
            {
                direction = SortDirection.Descending;
                sortingDirection = "DESC";
            }
            else
            {
                direction = SortDirection.Ascending;
                sortingDirection = "ASC";
            }
            DataView sortedView = new DataView(BindMasterSellingDetails());
            sortedView.Sort = e.SortExpression + " " + sortingDirection;
            Session["SortedView"] = sortedView;
            GridView1.DataSource = sortedView;
            GridView1.DataBind();
        }

        protected void GridView1_DataBound(object sender, EventArgs e)
        {
            for (int index = 0; index < GridView1.HeaderRow.Cells.Count; index++)
            {
                if (index == 1 || index == 2 || index == 3 || index == 4 || index == 5 || index == 6 || index == 7)
                {
                    var celll = GridView1.HeaderRow.Cells[index];
                    var headerText = GridView1.HeaderRow.Cells[index].Text;
              
                    Image image = new Image { ImageUrl = "../assets/img/UpDown50x50.png" };
                    image.Attributes.Add("style", "margin-left:1em;margin-right:0em;width:20px;height:20px;");
                    image.Visible = true;
                    celll.Controls.Add(image);
                }
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(sql))
            {
                if (TextBoxSearch.Text != null && TextBoxSearch.Text != "")
                {
                    string find = "select * from Selling where(DeliveredAt like '%' + @DeliveredAt + '%' or DeliveredBy like '%' + @DeliveredBy + '%' or Grade like '%' + @Grade + '%' or VesselName like '%' + @VesselName + '%' or NextPort like '%' + @NextPort + '%' or Date like '%' + @Date + '%')";
                    SqlCommand cmd = new SqlCommand(find, con);
                    cmd.Parameters.Add("@DeliveredAt", SqlDbType.NVarChar).Value = TextBoxSearch.Text;
                    cmd.Parameters.Add("@DeliveredBy", SqlDbType.NVarChar).Value = TextBoxSearch.Text;
                    cmd.Parameters.Add("@Grade", SqlDbType.NVarChar).Value = TextBoxSearch.Text;
                    cmd.Parameters.Add("@VesselName", SqlDbType.NVarChar).Value = TextBoxSearch.Text;
                    cmd.Parameters.Add("@NextPort", SqlDbType.NVarChar).Value = TextBoxSearch.Text;
                    cmd.Parameters.Add("@Date", SqlDbType.NVarChar).Value = TextBoxSearch.Text;

                    con.Open();
                    cmd.ExecuteNonQuery();
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds, "DeliveredAt");
                    da.Fill(ds, "DeliveredBy");
                    da.Fill(ds, "Grade");
                    da.Fill(ds, "VesselName");
                    da.Fill(ds, "NextPort");
                    da.Fill(ds, "Date");
                    GridView1.DataSource = ds;
                    GridView1.DataBind();
                    con.Close();
                }
                else
                {
                    GridView1.DataSource = null;
                    GridView1.DataBind();
                }
            }
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            AddConfirmDelete((GridView)sender, e);

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string Id = GridView1.DataKeys[e.Row.RowIndex].Value.ToString();
                GridView gvOrders = e.Row.FindControl("gvOrders") as GridView;
                GridView gvOrders1 = e.Row.FindControl("gvOrders1") as GridView;
              //  gvOrders.DataSource = GetData(string.Format("select top 3 * from BuyingList where Id='{0}'", Id));
                gvOrders.DataSource = GetData(string.Format("select * from Selling where Id='{0}'", Id));
                gvOrders1.DataSource = GetData(string.Format("select * from Selling where Id='{0}'", Id));
                gvOrders1.DataBind();
                gvOrders.DataBind();
            }
        }
        public static void AddConfirmDelete(GridView gv, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                foreach (DataControlField dcf in gv.Columns)
                {
                    if (dcf.ToString() == "CommandField")
                    {
                        if (((CommandField)dcf).ShowDeleteButton == true)
                        {
                            e.Row.Cells[gv.Columns.IndexOf(dcf)].Attributes.Add("onclick", "return confirm(\"Are you sure?\")");
                        }
                    }
                }
            }
        }
    }
}