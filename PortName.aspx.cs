﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;

namespace MigasLevelUp
{
    public partial class PortName : System.Web.UI.Page
    {
        string sql = ConfigurationManager.ConnectionStrings["OilGas"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text != null && TextBox1.Text != "")
                using (SqlConnection con = new SqlConnection(sql))
                {
                    con.Open();
                    SqlCommand cmd = con.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "insert into tblNextPort values('" + TextBox1.Text + "')";
                    cmd.ExecuteNonQuery();
                    TextBox1.Text = null;
                    Label2.Visible = true;
                    Label2.Text = "Successful Saved";
                    Label2.ForeColor = Color.Green;
                }
            else
            {
                Label2.Visible = true;
                Label2.Text = "There's no data Input";
                Label2.ForeColor = Color.Red;
            }
        }
    }
}