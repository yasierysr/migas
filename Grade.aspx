﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeBehind="Grade.aspx.cs" Inherits="MigasLevelUp.Grade" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
         .btngrade {
            width:120px;
            height:40px;
            margin-top:0.5em;
            margin-bottom:1em;
            color:white;
            background-color:lightskyblue;
            border:none;
        }
            .btngrade:hover {
                background-color: deepskyblue;
            }
        .gradediv {
            margin-bottom:1em;
            margin-left:2em;
            padding-top:11.5em;
        }
        .txtgrade {
            width:180px;
            height:40px;
            padding-left:0.5em;
            border:1px ridge lightgrey;
        }
            .txtgrade:hover {
                border:1px ridge cornflowerblue;
            }
        .lblgrade {
            margin-right:1em;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="gradediv">
        <table class="gradetable">
            <tr class="gradetr">
                <td><asp:Label ID="Label1" runat="server" Text="Grade" CssClass="lblgrade"></asp:Label></td>
                <td><asp:TextBox ID="TextBox1" runat="server" CssClass="txtGrade"></asp:TextBox></td>
            </tr>
            <tr>
                <td></td>
                <td><asp:Button ID="Button1" runat="server" Text="Save" CssClass="btngrade" OnClick="Button1_Click" /></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="Label2" runat="server"></asp:Label></td>
            </tr>
        </table>
    
        </div>
</asp:Content>
