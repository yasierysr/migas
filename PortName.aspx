﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeBehind="PortName.aspx.cs" Inherits="MigasLevelUp.PortName" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
         .btnnextport {
            width:120px;
            height:40px;
            margin-top:0.5em;
            margin-bottom:1em;
            color:white;
            background-color:lightskyblue;
            border:none;
        }
            .btnnextport:hover {
                 background-color: deepskyblue;
            }
        .nextportdiv {
            padding-top:11.5em;
            margin-bottom:1em;
            margin-left:2em;
        }
        .txtnextport {
            width:180px;
            height:40px;
            padding-left:0.5em;
            border:1px ridge lightgrey;
        }
            .txtnextport:hover {
            border:1px ridge cornflowerblue;
            }
        .lblnextport {
            margin-right:1em;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="nextportdiv">
        <table class="nextporttable">
            <tr class="nextporttr">
                <td><asp:Label ID="Label1" runat="server" Text="Port Name" CssClass="lblnextport"></asp:Label></td>
                <td><asp:TextBox ID="TextBox1" runat="server" CssClass="txtnextport"></asp:TextBox></td>
            </tr>
            <tr>
                <td></td>
                <td><asp:Button ID="Button1" runat="server" Text="Save" CssClass="btnnextport" OnClick="Button1_Click" /></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="Label2" runat="server"></asp:Label></td>
            </tr>
        </table>
    
        </div>
</asp:Content>
