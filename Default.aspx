﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Master.master" CodeBehind="Default.aspx.cs" Inherits="MigasWebForm.Default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
     <style>
         .wholecontent {
             width:100%;
             height: auto 0px;
            padding-top:11em;
         }
        .pikaSearch {
           height:40px;
           padding-left:0.2em; 
           border:1px solid dimgray;
           margin-right:0.2em;
           margin-left:72em;
        }
            .pikaSearch:hover {
                border:1px solid darkslateblue; 
            }
        .pikaBtnSearch {
            border:none;
            font-family:Opensan;
            font-size: large;
            border-radius:5px;
            background-color: ActiveBorder;
            width:120px;
            height:40px;
            margin-top:0.2em;
        }
            .pikaBtnSearch:hover {
                background-color:ActiveCaption;
            }
            @font-face {
   font-family: Opensan;
   src: url('../assets/fonts/OpenSans-Regular.ttf');
}
         .edititem {
             width:120px;
             height:40px;
             padding-left:0.5em;
             border:1px ridge lightgrey;
         }
             .edititem:hover {
                  border:1px ridge grey;
             }
    </style>
    <script src="assets/js/jquery-1.10.2.js"></script>
    <script src="assets/js/Jquery.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var gridHeader = $('#<%=GridView1.ClientID%>').clone(true);
            $(gridHeader).find("tr:gt(0)").remove();
            $('#<%=GridView1.ClientID%> tr th').each(function (i) {
                $("th:nth-child(" + (i + 1) + ")", gridHeader).css('width', $(this).width().toString() + "px");
            });
            $("#GHead").append(gridHeader);
            $('#GHead').css('position', 'absolute');
            $('#GHead').css('top', $('#<%=GridView1.ClientID%>').offset().top);
        });
        $(document).ready(function () {
            var gridHeader1 = $('#<%=GridView2.ClientID%>').clone(true);
             $(gridHeader1).find("tr:gt(0)").remove();
             $('#<%=GridView2.ClientID%> tr th').each(function (i) {
                $("th:nth-child(" + (i + 1) + ")", gridHeader1).css('width', $(this).width().toString() + "px");
            });
            $("#GHead1").append(gridHeader1);
            $('#GHead1').css('position', 'absolute');
            $('#GHead1').css('top', $('#<%=GridView2.ClientID%>').offset().top);
         });
    </script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Advanced Tables -->
    <section class="wholecontent">
    <div class="panel panel-default" id="coba">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#TabelSellingList"
                    style="line-height: 35px;" class="text-bold text-decoration-none">Tabel Selling List
                </a>
            </h4>
        </div>
        <div id="TabelSellingList" class="panel-body panel-collapse in">
            <div class="table-responsive">
                <asp:TextBox ID="TextBoxSearch" runat="server" CssClass="pikaSearch"></asp:TextBox><asp:Button ID="Button1" runat="server" Text="Search" OnClick="Button1_Click" CssClass="pikaBtnSearch" />
                <div id="GHead"></div>
                    <div style="height:350px; overflow:auto"> 
                <asp:GridView class="table table-striped table-bordered" ID="GridView1" runat="server" AutoGenerateColumns="false" CellPadding="11" CellSpacing="1" GridLines="None" ShowHeader="true"
                    HeaderStyle-CssClass="FixedHeader" AlternatingRowStyle-BackColor="SteelBlue" RowStyle-BackColor="LightBlue"
                     DataKeyNames="Id" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating"
                     OnSorting="GridView1_Sorting" AllowSorting="true" OnDataBound="GridView1_DataBound">
                    <Columns>
                         <asp:TemplateField HeaderText="ID" HeaderStyle-CssClass="penyu" ItemStyle-CssClass="bintanglaut" SortExpression="Id">
                            <ItemTemplate>
                                 <%#Eval("Id") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                   <%-- <Columns>
                        <asp:TemplateField HeaderText="ID" HeaderStyle-CssClass="penyu" ItemStyle-CssClass="bintanglaut" SortExpression="Id">
                            <ItemTemplate>
                                <%#Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>--%>
                    <Columns>
                        <asp:TemplateField HeaderText="Ref. No." HeaderStyle-CssClass="penyu" ItemStyle-CssClass="bintanglaut" SortExpression="RefNo">
                            <ItemTemplate>
                                <%#Eval("RefNo") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TxtRefNo" runat="server" CssClass="edititem" Text='<%#Bind("RefNo") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:TemplateField HeaderText="Date" HeaderStyle-CssClass="penyu" ItemStyle-CssClass="bintanglaut" SortExpression="Date">
                            <ItemTemplate>
                                <%#Eval("Date") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TxtDate" runat="server" CssClass="edititem" Text='<%#Bind("Date") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:TemplateField HeaderText="Buyer's Name" HeaderStyle-CssClass="penyu1" ItemStyle-CssClass="bintanglaut" SortExpression="BuyerName">
                            <ItemTemplate>
                                <%#Eval("BuyerName") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TxtBuyerName" runat="server" CssClass="edititem" Text='<%#Bind("BuyerName") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:TemplateField HeaderText="Grade/Product" HeaderStyle-CssClass="penyu" ItemStyle-CssClass="bintanglaut" SortExpression="Product">
                            <ItemTemplate>
                                <%#Eval("Product") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TxtProduct" runat="server" CssClass="edititem" Text='<%#Bind("Product") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:TemplateField HeaderText="Quantity" HeaderStyle-CssClass="penyu" ItemStyle-CssClass="bintanglaut" SortExpression="Quantity">
                            <ItemTemplate>
                                <%#Eval("Quantity") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TxtQuantity" runat="server" CssClass="edititem" Text='<%#Bind("Quantity") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:TemplateField HeaderText="Status" HeaderStyle-CssClass="penyu" ItemStyle-CssClass="bintanglaut" SortExpression="Status">
                            <ItemTemplate>
                                <%#Eval("Status") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TxtStatus" runat="server" CssClass="edititem" Text='<%#Bind("Status") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <Columns>
                        <asp:CommandField ButtonType="Image" ShowEditButton="true" ShowCancelButton="true" EditImageUrl="~/assets/img/edit.png" CancelImageUrl="~/assets/img/cancel.png" UpdateImageUrl="~/assets/img/update.png"  ControlStyle-Height="30px" ControlStyle-Width="30px" ItemStyle-Width="50px" ItemStyle-Height="50px" ItemStyle-CssClass="dugtrio" ControlStyle-CssClass="golduck" />
                    </Columns>
                    <Columns>
                        <asp:ButtonField ButtonType="Image" Text="Print" ControlStyle-Height="30px" ControlStyle-Width="30px" ImageUrl="~/assets/img/print1.png" ItemStyle-Width="50px" ItemStyle-Height="50px" ItemStyle-CssClass="dugtrio" ControlStyle-CssClass="golduck" />
                    </Columns>
                    <Columns>
                        <asp:CommandField ButtonType="Image" ShowDeleteButton="true" DeleteImageUrl="~/assets/img/delete1.png" ControlStyle-Height="30px" ControlStyle-Width="30px" ItemStyle-Width="50px" ItemStyle-Height="50px" ItemStyle-CssClass="dugtrio" ControlStyle-CssClass="golduck" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        </div>
        </div>
       
        <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#TabelSellingList"
                    style="line-height: 35px;" class="text-bold text-decoration-none">Tabel Buying List
                </a>
            </h4>
        </div>
         <div id="Div1" class="panel-body panel-collapse in">
            <div class="table-responsive">
                <asp:TextBox ID="TextBoxSearch1" runat="server" CssClass="pikaSearch"></asp:TextBox><asp:Button ID="Button2" runat="server" Text="Search" OnClick="Button2_Click" CssClass="pikaBtnSearch" />
                <div id="GHead1"></div>
                    <div style="height:350px; overflow:auto"> 
        <asp:GridView ID="GridView2" runat="server" class="table table-striped table-bordered" AutoGenerateColumns="false" CellPadding="11" CellSpacing="1" GridLines="None"
                        AlternatingRowStyle-BackColor="SteelBlue" RowStyle-BackColor="LightBlue" DataKeyNames="Id"
                        OnRowCancelingEdit="GridView2_RowCancelingEdit" OnRowEditing="GridView2_RowEditing" OnRowUpdating="GridView2_RowUpdating" OnRowDeleting="GridView2_RowDeleting" OnRowDataBound="GridView2_RowDataBound"
            AllowSorting="true" OnSorting="GridView2_Sorting" OnDataBound="GridView2_DataBound">
                         <Columns>
                            <asp:TemplateField HeaderText="ID" HeaderStyle-CssClass="penyu" ItemStyle-CssClass="bintanglaut" SortExpression="Id">
                                <ItemTemplate>
                                    <%#Eval("Id") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                       <%-- <Columns>
                            <asp:TemplateField HeaderText="ID" HeaderStyle-CssClass="penyu" ItemStyle-CssClass="bintanglaut" SortExpression="Id">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>--%>
                        <Columns>
                            <asp:TemplateField HeaderText="Ref. No." HeaderStyle-CssClass="penyu" ItemStyle-CssClass="bintanglaut" SortExpression="RefNo">
                                <ItemTemplate>
                                    <%#Eval("RefNo") %>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TxtRefNo1" runat="server" CssClass="edititem" Text='<%#Bind("RefNo") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Date" HeaderStyle-CssClass="penyu" ItemStyle-CssClass="bintanglaut" SortExpression="Date">
                                <ItemTemplate>
                                    <%#Eval("Date") %>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TxtDate1" runat="server" CssClass="edititem" Text='<%#Bind("Date") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Buyer's Name" HeaderStyle-CssClass="penyu1" ItemStyle-CssClass="bintanglaut" SortExpression="BuyerName">
                                <ItemTemplate>
                                    <%#Eval("BuyerName") %>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TxtBuyerName1" runat="server" CssClass="edititem" Text='<%#Bind("BuyerName") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Grade/Product" HeaderStyle-CssClass="penyu" ItemStyle-CssClass="bintanglaut" SortExpression="Product">
                                <ItemTemplate>
                                    <%#Eval("Product") %>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TxtProduct1" runat="server" CssClass="edititem" Text='<%#Bind("Product") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Quantity" HeaderStyle-CssClass="penyu" ItemStyle-CssClass="bintanglaut" SortExpression="Quantity">
                                <ItemTemplate>
                                    <%#Eval("Quantity") %>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TxtQuantity1" runat="server" CssClass="edititem" Text='<%#Bind("Quantity") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Status" HeaderStyle-CssClass="penyu" ItemStyle-CssClass="bintanglaut" SortExpression="Status">
                                <ItemTemplate>
                                    <%#Eval("Status") %>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TxtStatus1" runat="server" CssClass="edititem" Text='<%#Bind("Status") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:CommandField ButtonType="Image" ShowEditButton="true"  ShowCancelButton="true" EditImageUrl="~/assets/img/edit.png" CancelImageUrl="~/assets/img/cancel.png" UpdateImageUrl="~/assets/img/update.png" ControlStyle-Height="30px" ControlStyle-Width="30px" ItemStyle-Width="50px" ItemStyle-Height="50px" ItemStyle-CssClass="dugtrio" ControlStyle-CssClass="golduck" />
                        </Columns>
                        <Columns>
                            <asp:ButtonField ButtonType="Image" Text="Print" ImageUrl="~/assets/img/print1.png" ControlStyle-Height="30px" ControlStyle-Width="30px" ItemStyle-Width="50px" ItemStyle-Height="50px" ItemStyle-CssClass="dugtrio" ControlStyle-CssClass="golduck" />
                        </Columns>
                        <Columns>
                            <asp:CommandField ButtonType="Image" ShowDeleteButton="true" DeleteImageUrl="~/assets/img/delete1.png" ControlStyle-Height="30px" ControlStyle-Width="30px" ItemStyle-Width="50px" ItemStyle-Height="50px" ItemStyle-CssClass="dugtrio" ControlStyle-CssClass="golduck" />
                        </Columns>
                    </asp:GridView>
                </div>
             </div>
          </div>
            </div>
    </section>
    <!--End Advanced Tables -->
</asp:Content>
